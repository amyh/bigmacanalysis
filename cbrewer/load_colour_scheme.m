function cbtot = load_colour_scheme()

    [cb1] = cbrewer('seq','Reds',12,'pchip'); 
    [cb2] = cbrewer('seq','YlOrBr',12,'pchip'); 
    [cb3] = cbrewer('seq','YlGn',12,'pchip');
    [cb4] = cbrewer('seq','YlGnBu',12,'pchip'); 
    [cb5] = cbrewer('seq','Purples',12,'pchip');
    [cb6] = cbrewer('seq','PuRd',12,'pchip');
    [cbtot] = [cb1(8,:);cb2(8,:);cb3(8,:);cb4(8,:);cb5(8,:);cb6(8,:);];
    
end
