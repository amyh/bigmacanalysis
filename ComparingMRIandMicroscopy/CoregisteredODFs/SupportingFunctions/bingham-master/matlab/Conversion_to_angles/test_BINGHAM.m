%% DEMO
B = struct();
B.d = 3;
B.Z = -[100,10000000];
B.V = [1,0; 0,0; 0,1];


% To look up the normalization constant and its partial derivatives with
% respect to _Z_, use:

[B.F B.dF] = bingham_F(B.Z);
n=1000;


S=bingham_sample(B,n);

%% VISUALIZE
figure(1);
plot3(S(:,1),S(:,2),S(:,3),'r*'); shg
xlabel('x'); ylabel('y'); zlabel('z')
axis([-1 1 -1 1 -1 1]);



figure(2);
subplot(1,2,1); plot(S(:,1),S(:,2),'r*'); axis([-1 1 -1 1]);
xlabel('x'); ylabel('y')
subplot(1,2,2); plot(S(:,3),S(:,2),'g*'); axis([-1 1 -1 1]);
xlabel('x'); ylabel('z')
shg

%% CONVERSION TO ANGLES



B = struct();
B.d = 3;
B.V = [1,0; 0,0; 0,1];

kappa=(0:0.1:1000)';
kap2ang=repmat(kappa,[1,2]); kap2ang(:,2)=0;
n=1000;
for i=1:numel(kappa)
    B.Z = -[kappa(i),10000000];
    
    S=bingham_sample(B,n);
    ang=atand(abs(S(:,1))./abs(S(:,2)));
    ang=sort(ang,'ascend');
    
    
    kap2ang(i,2)=ang(n/2);
    if ~mod(kappa(i),10)
        fprintf(1,'Kappa = %4.0f is simulated.\n',kappa(i));
    end
end

%% SAVE

save('bingham_kappa2angle.mat','kap2ang','-ascii')


