function bingham_kap2ang_conversion(filename,mask_name)


kappa_vol=load_untouch_nii(filename); kappa=double(kappa_vol.img);
mask_vol=load_untouch_nii(mask_name); mask=logical(mask_vol.img);


kap2ang=load('bingham_kappa2angle.mat','-ascii');
kap_table=kap2ang(:,1); ang_table=kap2ang(:,2);
kappa_array=kappa(:);
kappa_mask=kappa_array(mask(:));
% Values above maximum kappa in table are set to that maximum kappa
kappa_mask(kappa_mask>max(kap2ang(:,1)))=max(kap2ang(:,1));

% Round kappa to 0.1 precision
kappa_bin=round(kappa_mask.*10)./10;
% Preallocate angles
ang_mask=zeros(size(kappa_mask));


% Get unique values for kappa and loop through each value
uni_kap=unique(kappa_bin);
for i=1:numel(uni_kap);
    table_id=kap_table==uni_kap(i);
    kap_id=kappa_bin==uni_kap(i);
    ang_mask(kap_id)=ang_table(table_id); 
end

ang_array=zeros(size(kappa_array));
ang_array(mask(:))=ang_mask;
angles=reshape(ang_array,size(kappa));

nii=kappa_vol; nii.img=angles;
filename_out=[filename(1:end-7) '_2angles.nii.gz'];
save_untouch_nii(nii,filename_out);




