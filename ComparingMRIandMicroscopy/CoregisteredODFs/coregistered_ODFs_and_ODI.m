%% Comparing microscopy and dMRI ODI in MR space

% - plot 2D microscopy fODF in an MR voxel. Microscopy can be pli or
% histology. Here we map to b10k postmortem MRI (1mm).
% - calculate ODI for microscopy orientations in MRI space
% - using output from ball and rackets model, project dMRI fODF onto the
% microscopy plane
% - overlay mri fODFs with that from PLI and histology
% - compare ODI from microscopy with MRI metrics (BAS ODI, FA and microFA)

% Here we have first mapped the microscopy data into the dwi MRI space
% using the scripts mentioned in ../TIRL
% These scripts are named TIRLmapping*
% They produce maps like:
% H090a_vect_in_dwi_1.0mm_b10kspace_x
% H090a_vect_in_dwi_1.0mm_b10kspace_y
% H090a_vect_in_dwi_1.0mm_b10kspace_z
% and
% H090a_norm_in_dwi_1.0mm_b10kspace_x
% H090a_norm_in_dwi_1.0mm_b10kspace_y
% H090a_norm_in_dwi_1.0mm_b10kspace_z
% which provide the x/y/z coordinates of the pixelwise structure tensor 
% orientation (vector) or the vector normal in b10k 1mm space

% Note, the fODF, ODF and FOD are used interchangably. Both refer to
% the fibre orientation distribution - i.e. how fibres are oriented within
% a 3D voxel or a 2D microscopy plane.

% Amy Howard
% FMRIB 2022

clear all, close all
addpath(genpath('../../../'))

% Set paths for fsl read_avw / write_avw
setenv( 'FSLDIR', '/usr/local/fsl');
fsldir = getenv('FSLDIR');
fsldirmpath = sprintf('%s/etc/matlab',fsldir);
path(path, fsldirmpath);
clear fsldir fsldirmpath;


%% Calculate the microscopy ODF within a given dMRI voxel [x,y,z]

% Define MRI voxel of interest
x = 34; y = 32; z = 46; % FSL coords, CC

% The FOD is defined as a histogram with the following bins
nbins = 90;
bincentres = linspace(0+.5*pi/nbins,pi-.5*pi/nbins,nbins);

% Load PLI data
pli = load_pli();
% function outputs:
% pli.inplane = pli inplane orientations mapped into dwi space
% pli.norm = pli normal mapped into dwi space
% pli.coords = coordinates of each pixel in dwi

% Extract fODF for PLI
pli = extract_fODF(pli,x,y,z,bincentres);
% function outputs:
% pli.ODF = frequency histogram with pli orientations
% pli.bincentres = histogram bins (0-pi)

% Calculate ODI for PLI ODF
pli = fit_dispersion(pli);
% function outputs: pli.ODI
disp(['Pli ODI = ' num2str(pli.ODI)])

% Plot
figure
ODF = pli.ODF/max(pli.ODF);    
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF]) 
legend('PLI')

%% Similarly for histology 

% Load histology data
histo = load_histo();
% Here we concatenate data from two consecutive histology slides
% function outputs:
% hist.inplane = as above, but now linearised [N,3]
% histo.norm = as above, but now linearised [N,3]
% histo.coords = as above, but now linearised [N,3]
% histo.hr1 = the number of rows in each image
% histo.hr2 = the number of columns in each image
% histo.ST = the full structure tensor output in native microscopy space

histo = extract_fODF(histo,x,y,z,bincentres);   

% In BigMac, we use the 50 micron structure tensor output for
% MRI-microscopy coregistration. For the histology FOD however, we want to
% include all fibre orientations from the original 0.3 micron data. These
% orientations are saved in the .mat file output from ST and can be
% included in the fODF by:
histo = histo_fODF_fullres(histo);

% Fit dispersion for this fODF
histo = fit_dispersion(histo);
disp(['Histology ODI = ' num2str(histo.ODI)])

% Plot
figure
ODF = histo.ODF/max(histo.ODF);    
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF]) 
legend('Histo')

%% Calculate MRI FOD from BAR on the 2D microscopy plane
% BAR  = the ball and rackets model. Here the FOD is described by a Bingham
% distribution and we fit a single fibre population per voxel.
BAR = load_BAR();

% Calculate 2D dispersion for MRI voxel [x,y,z]
% 2D pane defined by microscopy normal
BAR = BAR_dispersion_2D(BAR,x,y,z,pli,bincentres);
disp(['BAR ODI = ' num2str(BAR.ODI_2D)])

% Plot 
figure
ODF = BAR.ODF./max(BAR.ODF);                
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF])
legend('BAR')

%% Putting this together: overlay ODFs onto single plot
cbtot = load_colour_scheme();

% Generate mask of where microscopy lies in MRI space
micro_mask_wm = microscopy_masks(pli,histo);
% This can be helpful when selecting voxels of interest

% Pick a voxel (use FSL coordinates)
x = 38; y = 32; z = 44; % CC - midline
x = 34; y = 32; z = 46; % CC - off midline
x = 28; y = 32; z = 51; % CS - crossing fibres


% Find fODF in this voxel for each modality
pli = extract_fODF(pli,x,y,z,bincentres);
histo = extract_fODF(histo,x,y,z,bincentres);   
histo = histo_fODF_fullres(histo);
BAR = BAR_dispersion_2D(BAR,x,y,z,pli,bincentres);


% Plot PLI ODF
figure
ODF = pli.ODF/max(pli.ODF);     % Easier to compare if all FODs have max=1
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF],...
    'Color',cbtot(1,:),'LineWidth',2)
hold on

% Plot histology ODF
ODF = histo.ODF/max(histo.ODF);     
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF],...
    'Color',cbtot(4,:),'LineWidth',2)

% Plot diffusion BAR output
ODF = BAR.ODF./max(BAR.ODF);                
polarplot([bincentres,bincentres+pi,bincentres],[ODF,ODF,ODF],...
    'Color',cbtot(3,:),'LineWidth',2)

set(gca,'RTickLabel','');
set(gca,'ThetaTickLabel','');
legend('PLI','Histo','BAR')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compare metrics from MRI and microscopy across multiple voxels %%%%%%%%%

% This script calculates  MRI and microscopy estimates of ODI from the 
% white matter mask provided. As these voxelwise comparisons take a long
% time to compute, this white matter mask covers only a subset of voxels
% included in Figure 6 of the paper. For Figure 6 of the paper, the same
% code was run for a slab of MRI space covered by 30 consecutive PLI and
% histology images. Prebaked outputs are provided below.

% Load mask for ROI
mask = double(read_avw('./BigMac/CoregisteredODFs/Masks/WM_ROI.nii.gz'));

% Number of voxels in mask
ind = find(mask==1);
N = numel(ind);
disp(['Number of voxels in mask: ' num2str(N)])
% Note this is fairly slow for large N

% Initialise matrices for estimated ODI
histo_ODI = nan(1,N);
pli_ODI = nan(1,N);
BAR_ODI = nan(1,N);
dODF_2D_FA = nan(1,N);
dODF_3D_FA = nan(1,N);

output_filename = 'comparing_mri_microscopy.mat';

for i = 1:N
    
    % Convert voxel index for FSL x,y,z coords
    [x,y,z] = ind2sub(size(mask),ind(i));
    x = x-1; y = y-1; z = z-1; % FSL coords
    
    % Histology ODI
    histo = extract_fODF(histo,x,y,z,bincentres);   
    histo = histo_fODF_fullres(histo);
    histo = fit_dispersion(histo);
    histo_ODI(i) = histo.ODI;
       
    % PLI ODF
    pli = extract_fODF(pli,x,y,z,bincentres);
    pli = fit_dispersion(pli);
    pli_ODI(i) = pli.ODI;
    
    % BAR ODI
    BAR = BAR_dispersion_2D(BAR,x,y,z,pli,bincentres);
    BAR_ODI(i) = BAR.ODI_2D;
    
    disp(i)
    if mod(i,10)==0
        save(output_filename,'histo_ODI','pli_ODI','BAR_ODI','ind')
    end
    
end

disp('done')
save(output_filename,'histo_ODI','pli_ODI','BAR_ODI','ind')

% Note, this can take a long time to run (several hours). Instead you may
% wish to load the prebaked solution provided (i.e. continue running script
% below)

%% Compare output to FA and micro FA from dwi

% Create voxelwise scatter plots of MRI metrics versus microscopy ODI.
% Note option 2 and 3 below recreate figure 6c,d and supplmentary figure 6
% of our paper

%%% OPTION 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prebaked output from the block above
filename = './BigMac/CoregisteredODFs/Prebaked/comparing_mri_microscopy.mat';
mask = double(read_avw('./BigMac/CoregisteredODFs/Masks/WM_ROI.nii.gz'));
mask_cs = double(read_avw('./BigMac/CoregisteredODFs/Masks/CS_ROI.nii.gz'));
fa = double(read_avw(['./BigMac/MRI/Postmortem/dwi/b10k/1.0mm/' ...
    'data.dtifit/dti_FA.nii.gz']));
ufa = double(read_avw(['./BigMac/MRI/Postmortem/dwi/TensorEncoding/' ...
    '/b04k+b07k+b10k.merged/dtd_gamma_ufa_b10kspace.nii.gz']));
% % Note, ufa has been registered to this space


%%% OPTION 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prebaked output from 30 PLI and histology slides and postmortem dwi
% filename = './BigMac/CoregisteredODFs/Prebaked/postmortem_outputs.mat';
% mask = double(read_avw(['./BigMac/MRI/Postmortem/dwi/b10k/1.0mm/' ...
%     'masks/mask_wm.nii.gz']));
% mask_cs = double(read_avw('./BigMac/CoregisteredODFs/Masks/CS_ROI.nii.gz'));
% fa = double(read_avw(['./BigMac/MRI/Postmortem/dwi/b10k/1.0mm/' ...
%     'data.dtifit/dti_FA.nii.gz']));
% ufa = double(read_avw(['./BigMac/MRI/Postmortem/dwi/TensorEncoding/' ...
%     'b04k+b07k+b10k.merged/dtd_gamma_ufa_b10kspace.nii.gz']));


%%% OPTION 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Prebaked output from 30 PLI and histology slides and invivo dwi (prime-de)
% mask = double(read_avw(['./BigMac/MRI/Invivo/prime-de/dwi/masks/' ...
%     'mask_wm.nii.gz']));
% mask_cs = nan*mask;                         % do not have CS mask in vivo
% filename = './BigMac/CoregisteredODFs/Prebaked/invivo_outputs.mat';
% fa = double(read_avw(['./BigMac/MRI/Invivo/prime-de/dwi/data.dtifit/' ...
%     'dti_FA.nii.gz']));
% ufa = nan*fa;                % no estimates of micro FA available in vivo


%%%%%%%%%%%%%%%%%%%%%

% Load prebaked data
load(filename)

% Load colour map
cbtot = load_colours(); 

% Plot
figure('DefaultAxesFontSize',16)
scatter_plot(pli_ODI(mask==1),BAR_ODI(mask==1),...
    'PLI ODI','BAR ODI',cbtot(2,:),1);
scatter_plot(histo_ODI(mask==1),BAR_ODI(mask==1),...
    'Gallyas ODI','BAR ODI',cbtot(2,:),2);
scatter_plot(histo_ODI(mask==1),fa(mask==1),...
    'Gallyas ODI','FA',cbtot(2,:),3);
scatter_plot(histo_ODI(mask==1),ufa(mask==1),...
    'Gallyas ODI','microFA',cbtot(2,:),4);

% Hypothesise that correlation with microFA is due to partial voluming
% => also plot region from only centrum semiovale

% Plot
scatter_plot(pli_ODI(mask_cs==1),BAR_ODI(mask_cs==1),...
    'PLI ODI','BAR ODI',[0 0.7 0.7],5);
scatter_plot(histo_ODI(mask_cs==1),BAR_ODI(mask_cs==1),...
    'Gallyas ODI','BAR ODI',[0 0.7 0.7],6);
scatter_plot(histo_ODI(mask_cs==1),fa(mask_cs==1),...
    'Gallyas ODI','FA',[0 0.7 0.7],7);
scatter_plot(histo_ODI(mask_cs==1),ufa(mask_cs==1),...
    'Gallyas ODI','microFA',[0 0.7 0.7],8);
% Correlation disappears



%%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function stats = scatter_plot(x,y,xlab,ylab,c,Nsub)

    % Remove nans
    y(isnan(x)) = [];
    x(isnan(x)) = [];
    x(isnan(y)) = [];
    y(isnan(y)) = [];

    if numel(x)>0

    subplot(2,4,Nsub)

    % scatter plot. If >1000 points, use density plot
    if numel(x)<1000
        plot(x,y,'.','Color',c,'LineWidth',2,'MarkerSize',10)
        alpha(0.1)
    else
        dscatter(x,y)
        %colormap(spring)
        alpha(0.2)
    end
    xlabel(xlab)
    ylabel(ylab)

    axis square
    xlim([0,1])
    ylim([0,1])
    box on
    hold on

    % fit linear model
    mdl = fitlm(x,y);

    % beta values
    beta = mdl.Coefficients.Estimate;
    % correlation coefficient
    r = sqrt(mdl.Rsquared.Ordinary);
    if beta(2)<0, r=-r; end
    % stats
    stats = anova(mdl,'summary');
    p = stats.pValue(2);

    % calculate confidence boundaries
    % code from Boris Gutman (2023). Linear regression confidence 
    % interval (https://www.mathworks.com/matlabcentral/fileexchange/...
    % 39339-linear-regression-confidence-interval), 
    % MATLAB Central File Exchange. Retrieved March 1, 2023.

    conf = 0.05; % 0.95 confidence boundaries

    N = length(x);
    x_min = min(x);
    x_max = max(x);
    n_pts = 100;
    X = x_min:(x_max-x_min)/n_pts:x_max;
    Y = ones(size(X))*beta(1) + beta(2)*X;
    SE_y_cond_x = sum((y - beta(1)*ones(size(y))-beta(2)*x).^2)/(N-2);
    SSX = (N-1)*var(x);
    SE_Y = SE_y_cond_x*(ones(size(X))*(1/N + (mean(x)^2)/SSX) + ...
        (X.^2 - 2*mean(x)*X)/SSX);
    Yoff = (2*finv(1-conf,2,N-2)*SE_Y).^0.5;
    top_int = Y + Yoff;
    bot_int = Y - Yoff;

    %  plot line of best fit with confidence intervals
    if p<0.05
        dummyh = plot(X,Y,'k','LineWidth',1);
        xconf = [X,X(end:-1:1)];
        yconf = [bot_int,top_int(end:-1:1)];
        f = fill(xconf,yconf,'black');
        alpha(f,0.1)   
        f.EdgeColor = 'none';  
        legend(dummyh,sprintf('r=%0.2f',r))
    end

    fprintf('r=%0.2f, p=%f \n',r,p)

    if Nsub==4 || Nsub==8
        ylim([0.8,1.05])
    end

    end

end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function m = fit_dispersion(m)

    % Extract ODI from microscopy FOD
    % Fit Bingham distribution and convert kappa to ODI

    power = m.ODF./max(m.ODF);
    samples = sample_fod(m.bincentres,power,500);
    [X,Y] = pol2cart(samples(:),1);
    B = bingham_fit([X,Y,zeros(length(X),1)]);  
    m.k2 = abs(B.Z(2));
    m.ODI = 2/pi .* atan(1./m.k2);
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function sampled_alpha = sample_fod(alpha,power,n)

    % Author: Jeroen Mollink
    % FMRIB centre, University of Oxford
    % mollink.jeroen@gmail.com
    % Fit Bingham distribution to ODF

    % alpha : Angles
    % power : Power of angular frequency spectrum
    % n : number of samples to generate

    % Normalize power spectrum and get unit size of a single sample
    power = power./max(power);
    n_unit = sum(power)/n;

    % Assign number of samples per angle based on power
    P_n = round(power / n_unit);

    % Generate P_n(angle) samples
    alpha_cell = cell(1,numel(alpha));
    for i=1:numel(alpha)
        alpha_cell{i} = alpha(i) + (rand(1,P_n(i))-0.5)*0.1;
    end
    sampled_alpha = cell2mat(alpha_cell);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function m = extract_fODF(m,x,y,z,bincentres)

    % Calculate 2D microscopy ODF for a given MRI voxel
    
    % Convert x y z from FSL to matlab coords (i.e. from 0 index to 1 index)
    x = x+1; y = y+1; z = z+1;

    % Find microscopy pixels for voxel [x,y,z]
    c = reshape(floor(m.coords),[],3)';
    m.ind = find(all(c==[x;y;z]));      
    if numel(m.ind)==0
        error('No microscopy pixels in MR voxel')
        return 
    end

    % Convert inplane angle to azimuth
    %  - Rotate normal to align with z axis and convert cart 2 sph coords
    normal = reshape(m.norm,[],3)';
    normal = normal(:,m.ind);
    inplane = reshape(m.inplane,[],3)';
    inplane = inplane(:,m.ind);
    m.az = zeros(size(m.ind));
    m.inc = zeros(size(m.ind)); % Sanity check - should be zero
    for i = 1:numel(m.ind)

        [phi,th] = cart2sph(normal(1,i),normal(2,i),normal(3,i));
        th = pi/2-th;
        irot = rotz(phi)*roty(-th)*rotz(-phi)*inplane(:,i);

        [m.az(i),m.inc(i)] = cart2sph(irot(1,:),irot(2,:),irot(3,:));
    end

    % Generate freq histogram = fODF
    m.az(m.az<0) = m.az(m.az<0)+pi;   % enforce symmetry
    m.ODF = hist(m.az,bincentres);  % Frequency histogram
    m.ODF = m.ODF./sum(m.ODF);      % Normalise
    m.bincentres = bincentres;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function R = rotx(a)
    R = [ 1 0 0;
        0 cos(a) -sin(a);
        0 sin(a) cos(a);];
end

function R = roty(a)
    R = [cos(a) 0 sin(a);
        0 1 0;
        -sin(a) 0 cos(a);];
end

function R = rotz(a)
    R = [cos(a) -sin(a) 0;
        sin(a) cos(a) 0;
        0 0 1;];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function BAR = BAR_dispersion_2D(BAR,x,y,z,micro,bincentres)

    % Calculate 2D dispersion from ball and rackets Bingham distribution

    % Convert x y z from FSL to matlab coords (i.e. from 0 index to 1 index)
    x = x+1; y = y+1; z = z+1;

    % Use mean theta, phi, psi and k from BAR samples to define FOD
    theta = circ_mean(squeeze(BAR.theta(x,y,z,:)));
    phi = circ_mean(squeeze(BAR.phi(x,y,z,:)));
    psi = circ_mean(squeeze(BAR.psi(x,y,z,:)));
    k1 = squeeze(BAR.k1(x,y,z));
    k2 = squeeze(BAR.k2(x,y,z));
    
    % Reconstruct Bingham ODF in 3D
    % Rotation matrix
    R = rotz(-psi)*roty(-theta)*rotz(-phi);    

    % Bingham structure
    B = struct();
    B.d = 3;
    B.Z = [-k1,-k2]; % Concentration parameters
    B.V = [0,0;0,1;1,0];

    % Generate Bingham samples and rotate with R
    ODF = bingham_sample(B,100000);
    % Output ODF is along x. Rotate so it is instead along z
    ODF = (roty(-pi/2)*ODF')';
    % Rotate ODF according to theta, phi, psi
    ODF = (R'*ODF')';
    
    % Find average normal from microscopy
    norm = reshape(micro.norm,[],3)';
    norm = norm(:,micro.ind);
    [az,inc] = cart2sph(norm(1,:),norm(2,:),norm(3,:));
    [xx,yy,zz] = sph2cart(circ_mean(az'),circ_mean(inc'),1);
    n = [xx,yy,zz];
    
    % Convert ODF to 2D
    % - Rotate ODF so that normal aligns with z axis and extract x-y comps
    [phi,th] = cart2sph(n(1),n(2),n(3));
    th = pi/2-th;
    ODFrot = (rotz(phi)*roty(-th)*rotz(-phi)*ODF')';

    % Remove z comp and normalise
    ODF_2D = ODFrot(:,1:2);
    ODF_2D = [ODF_2D,zeros(length(ODF_2D),1)];

    % figure,subplot(1,3,1),scatter3(ODF(:,1),ODF(:,2),ODF(:,3))
    % view(phi,th)
    % axis square
    % subplot(1,3,2),scatter3(ODF_2D(:,1),ODF_2D(:,2),ODF_2D(:,3))
    % view(90,90)
    % axis square
    % title('projected')

    ODF_2D = ODF_2D./vecnorm(ODF_2D,2,2);

    % subplot(1,3,3),scatter3(ODF_2D(:,1),ODF_2D(:,2),ODF_2D(:,3))
    % view(90,90)
    % axis square
    % title('normalised')

    % Fit the 2D FOD
    B_2D = bingham_fit(ODF_2D);
    k2_2D = abs(B_2D.Z(2));  

    BAR.ODF_vect = ODF;
    BAR.ODF_2D_vect = ODF_2D;
    BAR.k2_2D = k2_2D;
    BAR.ODI_2D = 2/pi .* atan(1./k2_2D);
    
    % Convert 2D ODF into histogram
    [az,~] = cart2sph(ODFrot(:,1),ODFrot(:,2),ODFrot(:,3));
    % Ensure orientations are defined -pi/2-pi/2
    az(az<0) = az(az<0)+pi;
    az(az>pi) = az(az>pi)-pi;
    ODF = hist(az,bincentres);
    BAR.ODF = ODF./sum(ODF);    % Save normalised version
    BAR.bincentres = bincentres;
    
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function cbtot = load_colours()

    [cb1] = cbrewer('seq','Reds',12,'pchip'); 
    [cb2] = cbrewer('seq','YlOrBr',12,'pchip'); 
    [cb3] = cbrewer('seq','YlGn',12,'pchip');
    [cb4] = cbrewer('seq','YlGnBu',12,'pchip'); 
    [cb5] = cbrewer('seq','Purples',12,'pchip');
    [cb6] = cbrewer('seq','PuRd',12,'pchip');
    [cbtot] = [cb1(8,:);cb2(8,:);cb3(8,:);cb4(8,:);cb5(8,:);cb6(8,:);];

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function histo = load_histo(regspace)

    % Load histology data - orientations and coordinates in dwi space, as
    % well as structure tensor output, for better estimation of dispersion
    
    if ~exist('regspace','var')
        regspace = 'dwi_1.0mm_b10kspace';
    end
    
    % Concatenate data across multiple slides
    % Note data is linearised to account for difference in image size
    files = {'H090a','H091a'};
    
    histo.inplane = [];
    histo.norm = [];
    histo.coords = [];
    histo.hr1 = [];
    histo.hr2 = [];
    histo.ST.FOD = [];
    histo.ST.k1 = [];
    histo.ST.k2 = [];
    histo.ST.O = [];
    histo.ST.ODI1 = [];
    histo.ST.ODI2 = [];
    histo.ST.thumb = [];

    % Definition of bins from ST code
    bins = 90;
    histo.ST.bincentres = linspace(-pi/2+.5*pi/bins,pi/2-.5*pi/bins,bins);

    for i = [1,2]

        fhisto = files{i};
        ddir = ['./BigMac/Microscopy/Gallyas/Anterior/' fhisto(1:end-1) ...
            'x/structureTensor/' fhisto '_ST_150/'];
        f = [ddir '/Reg2MRI/VectorMapping/'];

        % Load in plane angle, normal and coordinates in dwi spacce
        tmp.inplane = load_vector([f '/' regspace '_inplane']);
        tmp.norm = load_vector([f '/' regspace '_norm']);
        % wm mask in dwi space
        mask_dwi = double(read_avw(...
            './BigMac/MRI/Postmortem/dwi/b10k/1.0mm/masks/mask_wm.nii.gz'));
        tmp.coords = load_coords([f '/' regspace '_coords'],mask_dwi);
        [tmp.hr1,tmp.hr2,~] = size(tmp.inplane);
        histo.inplane = cat(1,histo.inplane,reshape(tmp.inplane,[],3));
        histo.norm = cat(1,histo.norm,reshape(tmp.norm,[],3));
        histo.coords = cat(1,histo.coords,reshape(tmp.coords,[],3));
        histo.hr1 = [histo.hr1,tmp.hr1];
        histo.hr2 = [histo.hr2,tmp.hr2];

        % Also load structure tensor output
        tmp = load([ddir '/' fhisto '_struc_tensor.mat']);
        histo.ST.FOD = cat(1,histo.ST.FOD,reshape(tmp.FOD,[],90));
        histo.ST.k1 = cat(1,histo.ST.k1,tmp.k1(:));
        histo.ST.k2 = cat(1,histo.ST.k2,tmp.k2(:));
        histo.ST.O = cat(1,histo.ST.O,tmp.O(:));
        histo.ST.ODI1 = cat(1,histo.ST.ODI1,tmp.ODI1(:));
        histo.ST.ODI2 = cat(1,histo.ST.ODI2,tmp.ODI2(:));
        histo.ST.thumb = cat(1,histo.ST.thumb,tmp.thumb(:));

    end
    
    % Ensure orientations are defined -pi/2-pi/2
    histo.ST.O(histo.ST.O<0) = histo.ST.O(histo.ST.O<0)+pi;
    histo.ST.O(histo.ST.O>pi/2) = histo.ST.O(histo.ST.O>pi/2)-pi;

    % Change definition of orientations to be consistent with PLI
    % i.e. when an image 0-1 represents angless 0-pi
    histo.ST.O(histo.ST.O<0) = histo.ST.O(histo.ST.O<0)+pi;
    histo.ST.bincentres(histo.ST.bincentres<0) = ...
        histo.ST.bincentres(histo.ST.bincentres<0)+pi;
    % Now orientations are defined 0-pi as in PLI
    
    [histo.ST.bincentres,ind] = sort(histo.ST.bincentres,'ascend');
    histo.ST.FOD = histo.ST.FOD(:,ind);
    histo.ST.nbins = bins;
    
    disp(['Loading Gallyas data from: ' f '/' regspace])

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function histo = histo_fODF_fullres(histo)

    % Construct histo FOD in dwi space using high res fibre orientations
    
    % Find angle of rotation between mean orientation output from ST and 
    % that in dMRI space
    
    % Mean orientation for ST
    az = histo.ST.O(histo.ind)';
    
    % Angle of rotation
    ang = circ_dist(histo.az,az);

    % Generate new FOD which inludes all ST FODs, rotated according to TIRL
    % Rotate each FOD according to ang
    % For each pixel, caluclate how many bins need to move due to adding ang
    FOD = reshape(histo.ST.FOD,[],90)';
    FOD = FOD(:,histo.ind);
    binsz = histo.ST.bincentres(2)-histo.ST.bincentres(1);
    FODcorr = nan(size(FOD));   % corected FOD
    for i = 1:numel(ang)
        nbins = ceil((ang(i)-binsz/2)/binsz);  % have half a bin tolerance
        if nbins>0
            FODcorr(:,i) = [FOD(end-nbins+1:end,i);FOD(1:end-nbins,i)];
        else
            FODcorr(:,i) = [FOD(abs(nbins)+1:end,i);FOD(1:abs(nbins),i)];
        end
    end

    % Sum over FODs
    ODFfull = sum(FODcorr,2);

    % figure,polarplot([histo.ST.bincentres,histo.ST.bincentres+pi],[ODFfull,ODFfull])
    % hold on
    % polarplot([histo.ST.bincentres,histo.ST.bincentres+pi],[histo.ODF,histo.ODF])

    histo.ODF = ODFfull';
    histo.bincentres = histo.ST.bincentres;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mask = mask_in_mri(m,fout)

    % Output mask in MRI space which shows microscopy slice in 3D volume

    fdwi = './BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data.dtifit/dti_FA.nii.gz';
    c = unique(reshape(floor(m.coords),[],3),'rows')';
    data = read_avw(fdwi);
    mask = zeros(size(data));
    ind = sub2ind(size(data),c(1,:),c(2,:),c(3,:));
    mask(ind) = 1;
    save_avw(mask,fout,'f',[1,1,1])
    system(['fslcpgeom ' fdwi ' ' fout]);

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function BAR = load_BAR()

    % Load output from ball and rackets dwi model
    fBAR = './BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data.Ball_1_Racket_exvivo/';
    BAR.theta = double(read_avw([fBAR '/th_samples.nii.gz']));
    BAR.phi = double(read_avw([fBAR '/ph_samples.nii.gz']));
    BAR.psi = double(read_avw([fBAR '/psi_samples.nii.gz']));
    BAR.k1 = double(read_avw([fBAR '/k1_samples.nii.gz']));
    BAR.k2 = double(read_avw([fBAR '/k2_samples.nii.gz']));

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pli = load_pli(regspace)

    % Load PLI data in dwi space
    if ~exist('regspace','var')
        regspace = 'dwi_1.0mm_b10kspace';
    end

    fpli = 'P089x'; % PLI slice of interest
    d = './BigMac/Microscopy/PLI/Anterior/';
    f = [d fpli '/Reg2MRI/VectorMapping/'];
    
    % Load inplane, normal and coords mapped into dwi space
    pli.inplane = load_vector([f '/' regspace '_inplane']);
    pli.norm = load_vector([f '/' regspace '_norm']);
    mask_dwi = double(read_avw(...
        './BigMac/MRI/Postmortem/dwi/b10k/1.0mm/masks/mask_wm.nii.gz'));
    pli.coords = load_coords([f '/' regspace '_coords'],mask_dwi);
    [pli.hr1,pli.hr2,~] = size(pli.inplane);
    
    % Also load original orientations for reference
    pli.mosaic = double(imread([d fpli '/In_plane/mosaic.tif']))./255*pi;

    disp(['Loading PLI data from: ' f '/' regspace])
    

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mask_micro_wm = microscopy_masks(pli,histo)

    % Output mask of where microscopy lies in MR space
    % Find mask where voxels intersect
    pli.mask = mask_in_mri(pli,'./PLI_in_dwi.nii.gz');
    histo.mask = mask_in_mri(histo,'./Histo_in_dwi.nii.gz');
    mask_micro = pli.mask .* histo.mask;
    save_avw(mask_micro,'./Micro_in_dwi.nii.gz','f',[1,1,1]);
    system('fslcpgeom ./Histo_in_dwi.nii.gz ./Micro_in_dwi.nii.gz');

    % Select only white matter voxels
    mask_wm = double(read_avw(...
        './BigMac/MRI/Postmortem/dwi/b10k/1.0mm/masks/mask_wm.nii.gz'));
    mask_micro_wm = mask_micro.*mask_wm;
    save_avw(mask_micro_wm,'./Micro_in_dwi_wm.nii.gz','f',[1,1,1]);
    system('fslcpgeom ./Histo_in_dwi.nii.gz ./Micro_in_dwi_wm.nii.gz');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function v = load_vector(file)

    x = double(imread([file '_x.tif']));
    [s1,s2] = size(x);
    v = nan(s1,s2,3);
    v(:,:,1) = x;
    clear x
    v(:,:,2) = double(imread([file '_y.tif']));
    v(:,:,3) = double(imread([file '_z.tif']));

    % Set range -1 - 1
    v = v/255;
    v = v*2-1;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function c = load_coords(file,mri)

    x = double(imread([file '_x.tif']));
    [s1,s2,s3] = size(mri);

    c = nan(size(x,1),size(x,2),3);
    c(:,:,1) = x./255.*s1;

    clear x
    c(:,:,2) = double(imread([file '_y.tif']))./255.*s2;
    c(:,:,3) = double(imread([file '_z.tif']))./255.*s3;

    % Change from 0 indexing to 1 indexing
    c = c+1;

end

