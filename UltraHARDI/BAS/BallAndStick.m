%% Ball and stick on ultra-HARDI data

% Analyse the # of voxels with single, secondary and tertiary fibre 
% populations. How does this depend on b-value and angular resolution?

% Code to reproduce Figue 2b of the paper.

% Data has been previously processed using 
% bedpostx_single_shell.sh and bedpostx_cones.sh to calculate the cone of
% uncertainty. These scripts can be found in the folder fsl_scripts.

% - plot proportion of white matter voxels with >1, >2 or >3 fibres as a
% function of angular resolution
% - plot cone of uncertainty as a function of angular resolution


clear all, close all
addpath(genpath('../../'))
cbtot = load_colour_scheme();

%% Load ball and stick output

% BigMac directory
ddir = '../';
f = [ddir '/BigMac/MRI/Postmortem/dwi/'];

% Calculate fraction of voxels with single, double or teriary fibres
fibres = {'1','2','3'};
bvals = {'04','07','10'};
ndir = {'064','128','250','500','1000'};
frac = nan(numel(ndir),numel(bvals),numel(fibres));

for b = 1:numel(bvals)

    % Restrict analysis to white mattter
    if strcmp(bvals{b},'04')
        JJ = 3;
        mask = double(read_avw([f '/b04k/1.0mm/masks/mask_wm.nii.gz']));
    else    % Same mask for b=7,10
        JJ = numel(ndir);
        mask = double(read_avw([f '/b10k/1.0mm/masks/mask_wm.nii.gz']));
    end
    
    % Total number of white matter voxels
    N = sum(mask(:));

    % Loop through data
    for j = 1:JJ

        if j==JJ
            folder = 'data.bedpostX/';
        else
            folder = ['subset' ndir{j} '.bedpostX'];
        end
        for ff = 1:numel(fibres)
            % Load bedpostx signal fraction
            path = [f '/b' bvals{b} 'k/1.0mm/' folder];
            data = double(read_avw([path '/mean_f' fibres{ff} ...
                'samples.nii.gz']));
            
            % Threshold and mask out fibre populations that have very small
            % signal fraction or are not in wm
            thr = 0.01;      % threshold used for probtrackx
            data = data > thr;     
            data = data.*mask;
            
            % Calculate fraction
            frac(j,b,ff) = sum(data(:))/N;
            
            % Also calculate cone of uncertainty
            data2 = double(read_avw([path '/dyads' fibres{ff} ...
                '_cones95.nii.gz']));
            cone(j,b,ff,:) = data2(:).*data(:);
        end
    end
    
end

cone(cone==0) = nan;

%% Plot 

% Top row shows fraction of wm voxels with >1, >2 or >3 fibre populations
% as a function of angular resolution and b-value.
% Bottom row shows cone of uncertainty

figure
for i = 1:3
subplot(2,3,i)
hold on
plot(str2double(ndir),frac(:,1,i)*100,'.','MarkerSize',16,...
    'Color',cbtot(2,:))
plot(str2double(ndir),frac(:,2,i)*100,'.','MarkerSize',16,...
    'Color',cbtot(4,:))
plot(str2double(ndir),frac(:,3,i)*100,'.','MarkerSize',16,...
    'Color',cbtot(6,:))
axis square
box on
ylim([0 100])
yticks([0,25,50,75,100])
if i==3,legend(bvals), end

subplot(2,3,3+i)
hold on
run_multiple_boxplot(cone,ndir,i,cbtot([2,4,6],:))
axis square
box on
ylim([0,90])
yticks([0,30,60,90])

end


%%

function run_multiple_boxplot(cone,ndir,i,col)

A=squeeze(cone(:,1,i,:))';
B=squeeze(cone(:,2,i,:))';
C=squeeze(cone(:,3,i,:))';

% prepare data
data=cell(size(A,2),3);
for ii=1:size(data,1)
Ac{ii}=A(:,ii);
Bc{ii}=B(:,ii);
Cc{ii}=C(:,ii);
end
data=vertcat(Ac,Bc,Cc);

xlab = ndir;
col = [col([3,2,1],:),ones(size(col,1),1)];
col = [col([3,2,1],:),ones(size(col,1),1)];
multiple_boxplot(data',xlab,{'A', 'B', 'C'},col')

end


function multiple_boxplot(data,xlab,Mlab,colors)

% By Ander Biguri. Downloaded from:
% https://uk.mathworks.com/matlabcentral/fileexchange/47233-multiple_boxplot-m

% data is a cell matrix of MxL where in each element there is a array of N
% length. M is how many data for the same group, L, how many groups.
%
% Optional:
% xlab is a cell array of strings of length L with the names of each
% group
%
% Mlab is a cell array of strings of length M
%
% colors is a Mx4 matrix with normalized RGBA colors for each M.

% check that data is ok.
if ~iscell(data)
    error('Input data is not even a cell array!');
end

% Get sizes
M=size(data,2);
L=size(data,1);
% if nargin>=4
%     if size(colors,2)~=M
%         error('Wrong amount of colors!');
%     end
% end
% if nargin>=2
%     if length(xlab)~=L
%         error('Wrong amount of X labels given');
%     end
% end

% Calculate the positions of the boxes
positions=1:0.25:M*L*0.25+1+0.25*L;
positions(1:M+1:end)=[];

% x = str2double(xlab);
% positions = [x-20;x;x+20];
% positions = positions(:)';

% Extract data and label it in the group correctly
x=[];
group=[];
for ii=1:L
    for jj=1:M
        aux=data{ii,jj};
        x=vertcat(x,aux(:));
        group=vertcat(group,ones(size(aux(:)))*jj+(ii-1)*M);
    end
end
% Plot it
boxplot(x,group, 'positions', positions,'symbol','')%,'PlotStyle','compact');

% Set the Xlabels
aux=reshape(positions,M,[]);
labelpos = sum(aux,1)./M;

set(gca,'xtick',labelpos)
if nargin>=2
    set(gca,'xticklabel',xlab);
else
    idx=1:L;
    set(gca,'xticklabel',strsplit(num2str(idx),' '));
end
    

% Get some colors
if nargin>=4
    cmap=colors;
else
    cmap = hsv(M);
    cmap=vertcat(cmap,ones(1,M)*0.5);
end
color=repmat(cmap, 1, L);

% Apply colors
h = findobj(gca,'Tag','Box');
for jj=1:length(h)
   patch(get(h(jj),'XData'),get(h(jj),'YData'),color(1:3,jj)','FaceAlpha',color(4,jj));
end

% if nargin>=3
%     legend(fliplr(Mlab));
% end
end

function cbtot = load_colour_scheme()

    [cb1] = cbrewer('seq','Reds',12,'pchip'); 
    [cb2] = cbrewer('seq','YlOrBr',12,'pchip'); 
    [cb3] = cbrewer('seq','YlGn',12,'pchip');
    [cb4] = cbrewer('seq','YlGnBu',12,'pchip'); 
    [cb5] = cbrewer('seq','Purples',12,'pchip');
    [cb6] = cbrewer('seq','PuRd',12,'pchip');
    [cbtot] = [cb1(8,:);cb2(8,:);cb3(8,:);cb4(8,:);cb5(8,:);cb6(8,:);];
    
end
