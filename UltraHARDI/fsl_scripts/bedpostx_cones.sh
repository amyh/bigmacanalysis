#!/bin/bash

# Calculate cones of uncertainty for bedpostx dyads 

export FSLDIR=/opt/fmrib/fsltmp/fsl_FinalFive
folder=$1
thr=$2
for fib in 1 2 3; do
      ${FSLDIR}/bin/make_dyadic_vectors ${folder}/merged_th${fib}samples ${folder}/merged_ph${fib}samples ${folder}/mean_f${fib}samples ${folder}/dyads${fib} $thr
done
