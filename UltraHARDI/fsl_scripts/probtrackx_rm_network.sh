bdpx=$1;
Nsamples=$2;

if [ ! -e $bdpx/xfms/diff2standard.nii.gz ]; then
    echo Need to provide xfms/diff2standard and xfms/standard2diff. see reg folder.
    exit 0
fi

if [ -d $bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples} ]; then
    rm $bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples}/*;
else
    mkdir -p $bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples};
fi
command="$FSLDIR/bin/probtrackx2_gpu -x /vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/filenames.txt --network -V 1 -l -c 0.2 -S 2000 --steplength=0.2 -P $Nsamples --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 --forcedir --opd --ompl -s $bdpx/merged -m $bdpx/nodif_brain_mask --dir=$bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples} --xfm=$bdpx/xfms/standard2diff --invxfm=$bdpx/xfms/diff2standard --stop=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_interhemispheres.nii.gz --stop=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_neighbouringROIs.nii.gz"
fsl_sub -q cuda.q -N fdt_script -l $bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples}/ $command
echo $command > $bdpx/ConnectivityMatricies/RM_network/Nsamples${Nsamples}/command;
