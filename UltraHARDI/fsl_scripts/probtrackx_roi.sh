bdpx=$1;

if [ ! -e $bdpx/xfms/diff2standard.nii.gz ]; then
    echo Need to provide xfms/diff2standard and xfms/standard2diff. see reg folder.
    exit 0
fi
NSamples=5000

# Run separately for the areas of interest below, VACv, TCc, TCi

# VACv
area=VACv
ROI1=014
ROI2=114

# TCc
area=TCc
ROI1=008
ROI2=108

# TCi
area=TCi
ROI1=010
ROI2=110

command="/opt/fmrib/fsl/bin/probtrackx2_gpu10.2 \
    -x /vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI1}.nii.gz  -l \
    --onewaycondition -c 0.2 -S 2000 --steplength=0.2 -P $NSamples \
    --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 \
    --xfm=$bdpx/xfms/standard2diff.nii.gz \
    --invxfm=$bdpx/xfms/diff2standard.nii.gz \
    --stop=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI2}.nii.gz --forcedir --opd \
    -s $bdpx/merged \
    -m $bdpx/nodif_brain_mask  \
    --dir=$bdpx/ConnectivityMatricies/RM_network/${area}/ROI${ROI1}-${ROI2} \
    --opathdir \
    --waypoints=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI2}.nii.gz \
    --avoid=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_interhemispheres.nii.gz \
    --avoid=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_neighbouringROIs.nii.gz"

fsl_sub -q cuda.q -N ${area}-1 -l $bdpx/ConnectivityMatricies/RM_network/VACv/ROI${ROI1}-${ROI2}/ $command

# Run in both directions and afterwards sum output
command="/opt/fmrib/fsl/bin/probtrackx2_gpu10.2 \
    -x /vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI2}.nii.gz  -l \
    --onewaycondition -c 0.2 -S 2000 --steplength=0.2 -P $NSamples \
    --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 \
    --xfm=$bdpx/xfms/standard2diff.nii.gz \
    --invxfm=$bdpx/xfms/diff2standard.nii.gz \
    --stop=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI1}.nii.gz --forcedir --opd \
    -s $bdpx/merged \
    -m $bdpx/nodif_brain_mask  \
    --dir=$bdpx/ConnectivityMatricies/RM_network/${area}/ROI${ROI2}-${ROI1} \
    --opathdir \
    --waypoints=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/split/RM_inF99_${ROI1}.nii.gz \
    --avoid=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_interhemispheres.nii.gz \
    --avoid=/vols/Data/BigMac/MRI/Standard/F99/RM_atlas/mask_neighbouringROIs.nii.gz"

fsl_sub -q cuda.q -N ${area}-2 -l $bdpx/ConnectivityMatricies/RM_network/VACv/ROI${ROI1}-${ROI2}/ $command


# Afterwards combine outputs like:
# fslmaths ROI${ROI1}-${ROI2}/fdt_paths -add ROI${ROI2}-${ROI1}/fdt_paths ${area}_sum
# fslmaths ${area}_sum.nii.gz -div $((`cat ROI${ROI1}-${ROI2}//waytotal` + `cat ROI${ROI2}-${ROI1}/waytotal`)) ${area}_sum_density
