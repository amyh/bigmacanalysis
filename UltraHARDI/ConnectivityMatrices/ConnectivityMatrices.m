%% Connectivity Matrices

% How do the connectivity matrices change as a function of angular
% resolution?

% Recreate plots for Figure 2c of the paper.

% Here we have ran probtrackx to estimate the connectivity matrix between
% each region of interest in the RMM atlas. The command for this can be found
% in each bedpostx directory

% The aim here is the to see whether there are any noticable
% benefits of having data with ultra high angular resolution. 

% We hypothesise that higher angular resolution data will have improved 
% tracking in crossing fibre regions, resulting in more streamlines
% reaching the cortical ROIs (i.e an increase in streamline count in the
% connectivity matrix) and there being longer streamlines.
% Consequently, in this analysis we compare the number of streamlines
% estimated at each resolution, and the average length of these
% streamlines.

% - load connectomes for RM atlas at various angular resolutions
% - plot structural connectivity, plus difference matrix (1000-64 dir)
% - analyse streamline increase for homotopic regions
% - see whether increase is related to short/mid/long streamlines


clear all, close all
addpath(genpath('../../'))
cbtot = load_colour_scheme();
ddir = '../'; % Location of BigMac directory



%% Compare connectivity matrices between b10k 64 and 1000 directions

% Note, this may take several minutes to run

% Load connectome for 1000 directions
f1 = [ddir '/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data.bedpostX/',...
    'ConnectivityMatricies/RM_network/Nsamples5000/'];
c1 = load_connectome(f1,0);

% Load connectome for 64 directions
f2 = [ddir '/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/subset064.bedpostX/',...
    'ConnectivityMatricies/RM_network/Nsamples5000/'];
c2 = load_connectome(f2,0);


% Plot connectivity matrix for 1000 and 64 gradient directions
map = 1-(1:1000)'./1000.*cbtot(3,:);    % colourmap
figure
subplot(1,2,1)
imagesc(c2.x_symm,[0,1e5])      % c2.symm is the symmetric matrix
set(gca,'Colormap',map)
colorbar
axis square
title('64 grad dir')

subplot(1,2,2)
imagesc(c1.x_symm,[0,1e5])
set(gca,'Colormap',map)
colorbar
axis square
title('1000 grad dir')



%% Plot difference matrix
x = c1.x_symm; y = c2.x_symm; lims=[-1e5,1e5];
d = x-y;    % (1000-64 dir)

% Colour map
j = customcolormap(linspace(0,1,11), {'#68011d','#b5172f','#d75f4e',...
    '#f7a580','#fedbc9','#f5f9f3','#d5e2f0','#93c5dc','#4295c1',...
    '#2265ad','#062e61'});

figure
subplot(1,2,1)
imagesc(d,lims)
axis square
colormap(j)
colorbar
title('streamline #')

% Normalise by number of streamlines at 64 grad dir to see relative
% increase in streamlines
subplot(1,2,2)
imagesc(d./y,[-50,50])
axis square
colormap(j)
colorbar
title('relative difference')



%% Analyse whether there is increased connectivity between homotopic regions

% Plot relative streamline increase (x-y)./y for each homotopic region
dr = (c1.x_symm-c2.x_symm)./c2.x_symm;

% Matrix is organised such that the first half of ROI correspond to the
% left hemipshere, the second half are the right hemisphere. 
n = size(x,1)/2;
% regions 1 and n+1 are homotopic

x = [];
for i = 1:n
    x = [x,dr(i,i+n)];
end

% plot
figure
plot(1:n,x,'x','Color',cbtot(1,:),'LineWidth',2,'MarkerSize',8);
hold on
plot(1:n,zeros(size(x)),'--k');
axis square
xlim([1,n])
set(gca,'FontSize',14)
xlabel('roi')
ylabel('streamline increase')



%% Order x-axis according to ROI position along AP/IS/LR axis
% Load atlas 
RM99 = double(read_avw([ddir 'BigMac/MRI/Standard/F99/RM_atlas/RM_inF99.nii.gz']));

% Calculate mean ROI position along each axis
coords = nan(n,3);
for i = 1:n
    ind = find(RM99(:)==i+1);
    [xx,yy,zz] = ind2sub(size(RM99),ind);
    c = [xx,yy,zz];  
    coords(i,:) = mean(c,1);    
end
clear xx yy zz ind c

% Reorder x-axis
figure
for i = 1:3
    subplot(1,3,i)
    plot(coords(:,i),x,'x','Color',cbtot(1,:),'LineWidth',2,'MarkerSize',8);
    hold on
    xx = min(coords(:,i)):max(coords(:,i));
    plot(xx,zeros(size(xx)),'--k');
    axis square
    ylim([-10,150])
    xlim([min(coords(:,i)),max(coords(:,i))])
    set(gca,'FontSize',14)
    c = corrcoef(coords(:,i),x);
    legend(num2str(c(2,1)))

    if i==1, title('Medial -> Lateral'), end
    if i==2, title('Posterior -> Anterior'), end
    if i==3, title('Inferior -> Superior'), end
end

% See interesting relationship for x axis 




%% How does streamline length change as a function of angular resolution

% Note, this may take several minutes to run as loading data is a little
% time intensive

% Compare data with different numbers of gradient directions
ang = {'064','128','250','500','750'};

% Count number of streamlines where the average streamline length is either
% short (<=20), medium (20-50) or long (>50)
short = nan(size(ang));
mid = nan(size(ang));
long = nan(size(ang));
for a = 1:numel(ang)
    f2 = [ddir '/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/subset' ang{a},...
        '.bedpostX/ConnectivityMatricies/RM_network/Nsamples5000/'];
    c2 = load_connectome(f2,0);
    
    short(a) = sum((c2.lengths(:)<=20).*c2.x(:));
    mid(a) = sum((c2.lengths(:)>20 & c2.lengths(:)<=50).*c2.x(:));
    long(a) = sum((c2.lengths(:)>50).*c2.x(:));
      
end

% Add dir=1000 results
f1 = [ddir '/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data.bedpostX/',...
    'ConnectivityMatricies/RM_network/Nsamples5000/'];
c1 = load_connectome(f1,0);

short = [short,sum((c1.lengths(:)<=20).*c1.x(:))];
mid = [mid,sum((c1.lengths(:)>20 & c1.lengths(:)<=50).*c1.x(:))];
long = [long,sum((c1.lengths(:)>50).*c1.x(:))];


% Plot number of streamlines in each category as function of angular res
figure
subplot(1,2,1),hold on
plot([str2double(ang),1000],short)
plot([str2double(ang),1000],mid)
plot([str2double(ang),1000],long)
axis square
xlabel('# grad dir')
ylabel('# streamlines')
title('streamline #')
    
subplot(1,2,2),hold on
plot([str2double(ang),1000],(short-short(1))./short(1),'x','Color',...
    cbtot(2,:),'LineWidth',2)
plot([str2double(ang),1000],(mid-mid(1))./mid(1),'x','Color',...
    cbtot(4,:),'LineWidth',2)
plot([str2double(ang),1000],(long-long(1))./long(1),'x','Color',...
    cbtot(6,:),'LineWidth',2)
axis square
box on
legend('short','mid','long')
xlabel('# grad dir')
ylabel('streamline increase')
title('relative streamline increase')

% Left show how the total number of streamlines change for each category
% (short/mid/long). Right shows the streamline increase.s




%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function c = load_connectome(f,flag)

    if ~exist('flag','var')
        flag = 1;
    end

    % Load network matrix
    x = load([f '/fdt_network_matrix']);
    disp(['size x = ' num2str(size(x))])
    waytotal = load([f '/waytotal']);
    disp(['size waytotal = ' num2str(size(waytotal))])

    % Normalise 
    x_norm = x./waytotal;

    % Make symmetric (i.e. remove directionality)
    x_symm = nan(size(x));
    x_norm_symm = nan(size(x));
    for i = 1:size(x,1)
        for j = i:size(x,2)
            x_symm(i,j) = (x(i,j)+x(j,i))/2;
            x_symm(j,i) = x_symm(i,j);
            x_norm_symm(i,j) = (x_norm(i,j)+x_norm(j,i))/2;
            x_norm_symm(j,i) = x_norm_symm(i,j);
        end
    end

    % Load streamline lengths 
    lengths = load([f '/fdt_network_matrix_lengths']);

    % Make symmetric
    lengths_symm = nan(size(lengths));
    for i = 1:size(lengths,1)
        for j = i:size(lengths,2)
            lengths_symm(i,j) = (lengths(i,j)+lengths(j,i))/2;
            lengths_symm(j,i) = lengths_symm(i,j);
        end
    end

    % Create frequency histogram of streamline lengths
    % (i.e. for each entry, repeat the average streamline length by the 
    % number of streamlines)
    count = 1;
    for i = 1:numel(x)
        freq_hist(count:count+x(i)-1) = repmat(lengths(i),1,x(i));
        count = count+x(i);
    end

    % Save as a struct
    c.freq_hist = freq_hist;
    c.lengths = lengths;
    c.lengths_symm = lengths_symm;
    c.x = x;
    c.x_symm = x_symm;
    c.x_norm = x_norm;
    c.x_norm_symm = x_norm_symm;

    % Plot basic output
    if flag
        plot_basics(c)
    end

end

function plot_basics(c)

    % Plot basic outputs of connectivity matrices

    figure
    subplot(3,3,1)
    imagesc(c.x)
    title('output matrix')
    axis square

    subplot(3,3,2)
    imagesc(c.x_norm)
    title('normalised by waytotal')
    axis square

    subplot(3,3,3)
    imagesc(c.x_norm_symm)
    title('normalised & symmetric')
    axis square

    subplot(3,3,5)
    imagesc(c.lengths)
    title('av streamline length')
    disp(['size lengths = ' num2str(size(c.lengths))])
    axis square

    subplot(3,3,6)
    imagesc(c.lengths_symm)
    title('symmetric')
    axis square

    subplot(3,3,8)
    histogram(c.freq_hist,90)
    xlabel('streamline length')
    ylabel('# streamlines')
    axis square

    subplot(3,3,9)
    histogram(c.freq_hist,90)
    set(gca, 'yscale','log')
    xlabel('streamline length')
    ylabel('log 10 # streamlines')
    axis square

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ----------------------------------------------------------------------- %
% FUNCTION "customcolormap" defines a customized colobar given the        %
% positions and the colors that are going to generate the gradients.      %
%                                                                         %
%   Input parameters:                                                     %
%       - positions:    Vector of positions, from 0 to 1. Note that the   %
%                       first position must be 0, and the last one must   %
%                       be 1.                                             %
%       - colors:       Colors to place in each position. This parameter  %
%                       can be specified as a RGB matrix (n_colors x 3), or
%                       as a cell vector, containing HTML values.         %
%                       For instance: {'#ffffff','#ff0000','#000000'} is  %
%                       equivalent to [1 1 1; 1 0 0; 0 0 0].              %
%       - m:            (Optional) Number of points (recommended: m > 64).%
%                                                                         %
%   Output variables:                                                     %
%       - J:            Colormap in RGB values (dimensions [mx3]).        %
% ----------------------------------------------------------------------- %
%   Example of use:                                                       %
%       J = customcolormap([0 0.5 1], {'#ffffff','#ff0000','#000000'});   %
%       colorbar; colormap(J);                                            %
% ----------------------------------------------------------------------- %
%   Versions:                                                             %
%       - v1.0.:    (19/11/2018) Original script.                         %
% ----------------------------------------------------------------------- %
%       - Author:   V�ctor Mart�nez-Cagigal                               %
%       - Date:     19/11/2018                                            %
%       - Version:  1.0                                                   %
%       - E-mail:   vicmarcag (at) gmail (dot) com                        %
%                                                                         %
%       Biomedical Engineering Group (University of Valladolid), Spain    %
% ----------------------------------------------------------------------- %
function J = customcolormap(positions, colors, m)
    % Error detection and defaults
    if nargin < 3
       f = get(groot,'CurrentFigure');
       if isempty(f)
          m = size(get(groot,'DefaultFigureColormap'),1);
       else
          m = size(f.Colormap,1);
       end
    end
    if ~isnumeric(m), error('Parameter m must be numeric.'); end
    if nargin < 2, error('Not enough parameters.'); end
    
    if iscell(colors)
        colors = colors(:);
        n_colors = length(colors);
        for i = 1:n_colors
            temp = colors{i};
            if ~ischar(temp)
                error(['Colors must be specified in HEX format (e.g., #FFFFFF).' ...
                ' Type "help colorbar" for further information']);
            elseif ~strcmp(temp(1),'#')
                error(['Character # is missing if %s.' ...
                ' Type "help colorbar" for further information'], temp);
            elseif length(temp)~=7
                error(['Not a valid color format: %s (use this format: #FFFFFF).' ...
                ' Type "help colorbar" for further information'], temp);
            end
        end
    elseif ismatrix(colors)
        n_colors = size(colors);
        if length(n_colors) ~= 2
            error(['RGB colors must be a 2D matrix.' ...
                ' Type "help colorbar" for further information']);
        elseif n_colors(2) ~= 3
            error(['RGB colors matrix must have 3 columns.' ...
                ' Type "help colorbar" for further information']);
        elseif min(colors(:))<0 || max(colors(:))>255
            error(['RGB colors matrix values must range from 0 to 255.' ...
                ' Type "help colorbar" for further information']);
        end
    else
        error(['Colors must be a cell vector or a matrix of RGB values.' ...
            ' Type "help colorbar" for further information']);
    end
    if ~isvector(positions)
        error(['Positions must be specified as a vector.' ...
            ' Type "help colorbar" for further information']);
    elseif min(positions)<0 || max(positions)>1
        error(['Positions must range from 0 to 1 in an ascending order.' ...
            ' Type "help colorbar" for further information']);
    elseif length(positions) ~= length(unique(positions))
        error(['Check the positions vector, there are some duplicates.' ...
            ' Type "help colorbar" for further information']);
    else
        positions = sort(positions, 'ascend');
        if positions(1)~=0
            error(['The first positions must be 0.' ...
            ' Type "help colorbar" for further information']);
        elseif positions(length(positions))~=1
            error(['The last positions must be 1.' ...
            ' Type "help colorbar" for further information']);
        elseif length(positions) ~= n_colors
            error(['The number of positions does not match the number of colors.' ...
            ' Type "help colorbar" for further information']);
        end
    end
    % Convert HEX colors into RGB colors if required
    if iscell(colors)
        hex_colors = colors;
        colors = NaN(n_colors,3);
        for i = 1:n_colors
            colors(i,:) = hex2rgb(hex_colors{i});
        end
    end
    
    % Compute positions along the samples
    color_samples = round((m-1)*positions)+1;
    % Make the gradients among colors
    J = zeros(m,3);
    J(color_samples,:) = colors;
    diff_samples = diff(color_samples)-1;
    for d = 1:1:length(diff_samples)
        if diff_samples(d)~=0
            color1 = colors(d,:);
            color2 = colors(d+1,:);
            G = zeros(diff_samples(d),3);
            for idx_rgb = 1:3
                g = linspace(color1(idx_rgb), color2(idx_rgb), diff_samples(d)+2);
                g([1, length(g)]) = [];
                G(:,idx_rgb) = g';
            end
            J(color_samples(d)+1:color_samples(d+1)-1,:) = G;
        end
    end
    J = flipud(J);
end
% Function that converts an HEX string (format: #FFFFFF) to the
% corresponding RGB color
function rgb = hex2rgb(hexString)
	if size(hexString,2) ~= 7
		error('Not a color! %s', hexString);
	else
		r = double(hex2dec(hexString(2:3)))/255;
		g = double(hex2dec(hexString(4:5)))/255;
		b = double(hex2dec(hexString(6:7)))/255;
		rgb = [r, g, b];
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


