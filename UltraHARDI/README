Here we have scripts to reproduce the Figure 2 of the paper.
    
The ball and stick model was ran on data with different b-values and 
number of gradient directions. Here we decrease the number of gradient
drections by taking a subset of the ultra-HARD data for each b-value.

BAS looks at how the number of voxels classified as single/double/tertiary
fibres from the ball and stick model changes as a function of b-value
and angular resolution (Fig 2b).

Probtrackx was then run on the ball and stick output to estimate
the number of streamlines connecting each ROI of the RM cortical atlas.
The atlas includes 41 regions per hemisphere. Masks were used to stop
streamlines from jumping between cortical regions without entering the 
white matter, or jumping between hemispheres.

ConnectivityMatrices investigates differences in the ROI by ROI 
connectivity matrices as a function of angular resolution (Fig 2c).

Xtract was also run on b10k 1mm data with 1000 or 64 gradient directions
and b04k 0.6mm data with 128 gradient directions. Figure 2d compares the
reconstruction of slf2_r. Here we first normalise the sum_density image
by the sum_waytotal, and set a threshold of 0.01.

Tract reconstruction between individual ROIs was also performed for 
various ROIs of interest (VACv, TCc, TCi, see Supp Material for details).

The data to run this analysis is provided via the ORA doi given on the 
main page (UltraHARDI-data.zip).

Amy Howard
FMRIB 2022
