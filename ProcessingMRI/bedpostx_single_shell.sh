#!/bin/bash

# Run bedpostx for postmortem single shell data with model 1, dot compartment, and rician noise modelling

module add fsl_sub
folder=$1
bedpostx $folder -model 1 --f0 --ardf0 --rician
