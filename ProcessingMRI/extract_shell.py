#!/usr/bin/env fslpython
"""

Extract single shell from dwi data

Created on Fri Nov 29 10:54:42 2019

@author: amyh
"""
 
import numpy as np
import nibabel as nib
import os.path as op
import argparse


def main():
    p = argparse.ArgumentParser(description='Extract single shell from dwi data. Default extract b0s.')

    p.add_argument('-d','--data',
                  required=True,type=str,metavar='VOLUME',
                  help='input data')
    p.add_argument('-b','--bval',
                   required=True,type=str,metavar='<str>',
                   help='bvals text file')
    p.add_argument('-v','--bvecs',
                   required=True,type=str,metavar='<str>',
                   help='bvecs text file')
    p.add_argument('-s','--shell',metavar='N', type=int,
                   required=False,default=0,
                   help='b-value of shell. Default: b=0')
    p.add_argument('-o','--output',
                   required=True,type=str,metavar='VOLUME',
                   help='output data')
    p.add_argument('-a','--approx',metavar='N', type=int,
                   required=False,default=250,
                   help='range of equal bvals (default=250)') # Changed from 5


    args = p.parse_args()
    
    # Load data
    filename = op.expandvars(args.data)
    imobj    = nib.load(filename,mmap=False)
    im       = imobj.get_fdata().astype(float)
    b        = np.loadtxt(args.bval)
    v        = np.loadtxt(args.bvecs)
    oname    = op.expandvars(args.output)


    # round bvalues
    if args.approx>0:
        br = np.round(b/(2.0*args.approx))*2*args.approx
        
    # Extract b0s
    print('Shells detected')
    print(np.unique(br))
    m = min(np.unique(br), key=lambda x:abs(x-args.shell))
    print('Extracting shell b=')
    print(m)
    o = im[:,:,:,br==m].copy()

    # Save new image
    if oname[-2::]=='gz':
        dname = oname
        vname = oname[:-7:]+'.bvecs'
        bname = args.output[:-7:]+'.bvals'
    else:
        dname = oname+'/data.nii.gz'
        vname = oname+'/bvecs'
        bname = oname+'/bvals'
    
    newhdr = imobj.header.copy()
    newobj = nib.nifti1.Nifti1Image(o, None, header=newhdr)
    nib.save(newobj,dname)
    
    # Also save bvals and bvecs
    if v.shape[1]==3:
        np.savetxt(vname,v[br==m,:],fmt='%.6f')
        np.savetxt(bname,b[br==m],fmt='%.d')
    else:
        np.savetxt(vname,v[:,br==m],fmt='%.6f')
        np.savetxt(bname,b[br==m].reshape((1,-1)),fmt='%.d')
          


if __name__ == '__main__':
    main()

    
