#!/bin/bash

bdpx=$1
out=$2

if [ ! -e $bdpx/xfms/diff2standard.nii.gz ]; then
    echo Need to provide xfms/diff2standard and xfms/standard2diff. see reg folder.
    echo failed to read $bdpx/xfms/diff2standard.nii.gz
    exit 0
fi

mkdir -p $out/xtract/native
mkdir -p $out/xtract/standard
echo --steplength=0.2 > $out/xtract/ptx_options.txt

xtract -bpx $bdpx -out $out/xtract/native -species MACAQUE -gpu -ptx_options $out/xtract/ptx_options.txt -native
xtract -bpx $bdpx -out $out/xtract/standard -species MACAQUE -gpu -ptx_options $out/xtract/ptx_options.txt
