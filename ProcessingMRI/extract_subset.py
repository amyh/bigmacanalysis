#!/usr/bin/env fslpython
"""

Extract subeset of gradient directions from dwi data

Created on Fri Nov 29 10:54:42 2019

@author: amyh
"""
 
import numpy as np
import nibabel as nib
import os.path as op
import argparse


def main():
    p = argparse.ArgumentParser(description='Extract subset of gradient directions from dwi data. [lower bound:lower bound + nvols]. 0 indexed')

    p.add_argument('-d','--data',
                  required=True,type=str,metavar='VOLUME',
                  help='input data')
    p.add_argument('-b','--bval',
                   required=True,type=str,metavar='<str>',
                   help='bvals text file')
    p.add_argument('-v','--bvecs',
                   required=True,type=str,metavar='<str>',
                   help='bvecs text file')
    p.add_argument('-l','--lb',metavar='N', type=int,
                   required=True,
                   help='lower bound of subset')
    p.add_argument('-n','--nvol',metavar='N', type=int,
                   required=True,
                   help='number of volumes after lb')
    p.add_argument('-o','--output',
                   required=True,type=str,metavar='VOLUME',
                   help='output data')


    args = p.parse_args()
    
    # Load data
    filename = op.expandvars(args.data)
    imobj    = nib.load(filename,mmap=False)
    im       = imobj.get_fdata().astype(float)
    b        = np.loadtxt(args.bval)
    v        = np.loadtxt(args.bvecs)
    l        = args.lb
    n        = args.nvol
    oname    = op.expandvars(args.output)
        
    # Extract subset grad dir
    o = im[:,:,:,l:l+n].copy()

    # Save new image
    if oname[-2::]=='gz':
        dname = oname
        vname = oname[:-7:]+'.bvecs'
        bname = args.output[:-7:]+'.bvals'
    else:
        dname = oname+'/data.nii.gz'
        vname = oname+'/bvecs'
        bname = oname+'/bvals'
    
    newhdr = imobj.header.copy()
    newobj = nib.nifti1.Nifti1Image(o, None, header=newhdr)
    nib.save(newobj,dname)
    
    # Also save bvals and bvecs
    if v.shape[1]==3:
        np.savetxt(vname,v[l:l+n,:],fmt='%.6f')
        np.savetxt(bname,b[l:l+n],fmt='%.d')
    else:
        np.savetxt(vname,v[:,l:l+n],fmt='%.6f')
        np.savetxt(bname,b[l:l+n].reshape((1,-1)),fmt='%.d')
          


if __name__ == '__main__':
    main()

    
