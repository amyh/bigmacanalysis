# Re-run flag
RERUN=False

#./DO <dir>
# e.g. /vols/Data/BigMac/MRI/Invivo/last-scan/func/raw/preprocessing/

#if [ "$1" == "" ];then
#    echo "Usage DO.sh <monkey>"
#    exit 1
#fi

# Pick a monkey
#monk=$1

echo "--- Processing functional for $1"

# Create output folder
#D=/vols/Scratch/saad/monkeyRS/$monk
#mkdir -p $D
D=$1

#if [ ! -f $D/raw.nii.gz ];then
#    echo "Copy data"
#    cp /vols/Scratch/neichert/monkeyRS/$monk/functional/raw.nii.gz $D
#    cp /vols/Scratch/neichert/monkeyRS/$monk/structural/struct.nii.gz $D
#    cp /vols/Scratch/neichert/monkeyRS/$monk/structural/struct_brain.nii.gz $D
#else
#    echo "--skip copying data"
#fi

# ----------------------------------
# FIX ORIENTATION INFO 
# (NEED TO WAIT FOR MATT/PAUL TO FIX BUG)
#img=$D/struct
#imcp $img ${img}_tmp
#fslorient -deleteorient ${img}_tmp
#fslswapdim ${img}_tmp x z -y ${img}_tmp 
#fslorient -setqformcode 1 ${img}_tmp
# ----------------------------------


# Run melodic
if [ "$RERUN" == True ] || [ ! -d $D/func.ica ];then
    echo "Run melodic"
    rm -rf $D/func.ica
    melodic -i $D/func --nobet --verbose
else
    echo "--skip first melodic"
fi

# Get brain mask
if [ ! -f $D/fmri_mask.nii.gz ];then
    echo "Get brain mask in FMRI space"
    # resample_image needs fslpy to be installed 
    $FSLDIR/bin/resample_image -r $D/func.ica/mean $D/struct_brain $D/fmri_mask
    fslmaths $D/fmri_mask -bin $D/fmri_mask
else
    echo "--skip brain masking"
fi

# Filter out noise components and re-run melodic
remove_brackets(){
	echo $* | sed s@"\["@@g | sed s@"\]"@@g | sed s@" "@@g
}

if [ "$RERUN" == True ] || [ ! -d $D/func_filt.ica ];then
    # This just removes the annoying brackets and spaces from the fsleyes labels file
    echo "filter out bad components"
    if [ ! -f $D/func.ica/component_classification.txt ];then
	echo "NEED TO CREATE COMPONENT CLASSIFICATION FILE : $D/func.ica/component_classification.txt"
	echo "Run the below command to classify the components (into signal or noise) and save the results then run this script again:"
	echo ""
	#echo 'overlay1="$D/struct.nii.gz -dr 0 1000"'
	#echo 'overlay2="$D/func.ica/melodic_IC -dr 3 10 -cm red-yellow -nc blue-lightblue"'
	#echo "fsleyes -s melodic $overlay2 $overlay1"
	echo "fsleyes -s melodic -ad func.ica"
	echo ""
	exit 1
    fi
    bad_comps=`cat $D/func.ica/component_classification.txt | tail -n 1`
    fsl_regfilt -i $D/func -o $D/func_filt -d $D/func.ica/melodic_mix -f "`remove_brackets $bad_comps`" -v

    # Run melodic again 
    echo "rerun melodic"
    rm -rf $D/func_filt.ica
    melodic -i $D/func_filt --nobet --verbose -m $D/fmri_mask
else
    echo "--skip filtering and second ica"
fi


# Registration

F99=/vols/Scratch/saad/atlases/F99_atlas
out=$D/xfms
mkdir -p $out
if [ ! -f ${out}/anat_to_F99_nonlin.nii.gz ];then
    # -------- REG-to-F99 -------- #
    # 1. Registration to F99
    
    conf=$F99/config
    atl=$F99/mri/struct_brain
    anat=$D/struct_brain
    echo ""
    echo "  ---> Initial affine registration"
    flirt -in $anat -ref $atl -omat ${out}/anat_to_F99.mat -out ${out}/anat_to_F99_lin
    echo ""
    echo "  ---> FNIRT with custom config file"
    fnirt --in=$anat --ref=$atl --aff=${out}/anat_to_F99.mat --iout=${out}/anat_to_F99_nonlin --cout=${out}/anat_to_F99_warp  --config=${conf}
    invwarp -w ${out}/anat_to_F99_warp -r $anat -o ${out}/F99_to_anat_warp
    echo ""
    echo "  ---> Check registration"
    applywarp -i $anat -o ${out}/anat_to_F99_nonlin -r $atl -w ${out}/anat_to_F99_warp
else
    echo "--skip nonlinear reg"
fi

echo "here"

if [ ! -f $D/func2f99_reg.png ];then
    echo "FUNC -> STR -> F99"
    flirt -in $D/func.ica/mean.nii.gz -ref $D/struct_brain -applyxfm -usesqform -omat $D/xfms/func2str.mat
    applywarp -i $D/func.ica/mean -o $D/func.ica/mean_F99 -r ${F99}/mri/struct1mm_brain -w ${out}/anat_to_F99_warp  --premat=$D/xfms/func2str.mat
    fsleyes render --outfile $D/func2f99_reg.png $F99/mri/struct1mm.nii.gz -dr 0 100 $D/func.ica/mean_F99 -dr 200 1000 -cm red-yellow
    echo "--check registration by running: "
    echo "display $D/func2f99_reg.png"
else 
    echo "--skip lin reg"
fi


if [ ! -f $D/func_filt_F99.nii.gz ];then
    #fsl_sub -q bigmem.q -N func2f99 -l func_filt.ica 
    echo "resample the data "
    applywarp -i $D/func_filt -o $D/func_filt_F99 -r ${F99}/mri/struct1mm_brain -w ${out}/anat_to_F99_warp  --premat=$D/xfms/func2str.mat
else
    echo "--skip data resampling"
fi

if [ ! -f $D/func_filt_F99_2mm.nii.gz ];then
    echo "downsample the data"
    flirt -in $D/func_filt_F99 -out $D/func_filt_F99_2mm -ref  ${F99}/mri/struct1mm_brain -applyisoxfm 2
else
    echo "--skip downsampling"
fi


echo "Done."




