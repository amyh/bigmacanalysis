#!/usr/bin/env bash
set -e    # stop immediately on error
umask u+rw,g+rw # give group read/write permissions to all new files

# a wrapper to run topup and eddy
#--------------------------------------------------------------------------
# version history
# 2017-03-03  Lennart   added option to skip bias-correction before bet
# 2016-03-01	Lennart		polished
# 2016-03-01  Rogier    ruler of the universe
# 2013-01-01  Stam      created
#
# based on the file: DiffPreproc.sh
# Stamatios Sotiropoulos, Analysis Group, FMRIB Centre, 2013.
#
# copyright
# Stamatios Sotiropoulos, Lennart Verhagen, and Rogier B. Mars
# University of Oxford & Donders Institute, 2016-03-01
#--------------------------------------------------------------------------


#==========================================
# Edit this part
#==========================================

# data root directory
#rootdir=/vols/Data/rbmars/social_macaquediffusion
rootdir=		# Simply pass directory as first argument


# if MRCATDIR is not defined in the environment
[[ -z $MRCATDIR ]] && export MRCATDIR=$(cd ..; pwd)


#==========================================
# Sort the input options (probably don't need to edit this part)
#==========================================

usage() {
cat <<EOF

Preprocessing Pipeline for diffusion MRI. It extracts b0 images, estimates
susceptibility induced distortions (topup), estimates eddy-current artefacts
(eddy), and corrects for those together (eddy).

usage: topupeddy_wrapper.sh <dir> <flgBET> <overwrite>
  <dir> : working directory. expects data in <dir>/raw
  <flgBET> : a single integer, or a list of comma-separated integers, as in:
    "0,0,1" where the flag value indicates how to do the brain extraction,
    the <subjname> and <flgBET> inputs should match
      [0]: no bias corection, just two times bet_macaque
      [1]: two times bias correction and bet_macaque
      [2]: copy brainmask from dMRI-to-T1 registration for both times
  <overwrite> : = 1 to overwrite current topupeddy directory, = 0 to continue with current output

EOF
}

# give usage if no input is provided
[[ $# -eq 0 ]] && usage && exit 0

# retrieve the subject name(s) and BET flags from the input arguments; replace
# commas by spaces and make into an array to allow indexing in the loop
subjlist=(${1//,/ })
flgBETlist=(${2//,/ })
overwrite=$3

# By default, do not overwrite
if [[ -z $overwrite ]]; then
  overwrite=0;
fi 

# fill in some default values for flgBETlist
if [[ -z $flgBETlist ]] ; then
  flgBET=1
  # repeate the flgBET for all monkeys in the subjlist
  flgBETlist=($(printf 'X %.0s' ${!subjlist[@]} | tr "X" "$flgBET"))
fi

# script directory (where this script is stored)
#scriptsdir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/topupeddy"
scriptsdir=$MRCATDIR/pipelines/topupeddy

# hard-Coded variables for the pipeline
b0dist=10     #Minimum distance in volumes between b0s considered for preprocessing
b0dist=1  # AH
b0maxbval=50  #Volumes with a bvalue smaller than that will be considered as b0s

# cluster computating settings
MaxOPENMP_Nodes=4
if [ "x$SGE_ROOT" = "x" ] ; then
    if [ -f /usr/local/share/sge/default/common/settings.sh ] ; then
	. /usr/local/share/sge/default/common/settings.sh
    elif [ -f /usr/local/sge/default/common/settings.sh ] ; then
	. /usr/local/sge/default/common/settings.sh
    fi
fi


#==========================================
# Do the work (don't edit this part)
#==========================================
echo ""; echo "START: topup and eddy pipeline"
echo "$(date)"; echo ""

# loop over subjects
for idx in ${!subjlist[@]} ; do

  # extract info from array
  subj=${subjlist[$idx]}
  flgBET=${flgBETlist[$idx]}
  echo "  monkey: $subj"

  # assign directories
  WD=$rootdir/preprocessing/$subj/diffusion
  WD=$subj/

  # list images
  imgs=$(ls $WD/raw/*.nii.gz)

  # copy and rename bvecs and vals
  #for img in $imgs ; do
  #  cp $WD/raw/bvecs ${img%%.*}.bvec
  #  cp $WD/raw/bvals ${img%%.*}.bval
  #done

  # find phase encoding directions
  pedir=$(echo "$imgs" | grep -o '[FHLRAP]*[_0-9]*.nii.gz' | cut -d'.' -f1 | cut -d'_' -f1 | sort | uniq)
  [[ $(echo $pedir) == "LR RL" ]] && pedircode=1 || pedircode=2

  # define arbitrary positive (RL/AP/HF) and negative directions (LR/PA/FH)
  pedirPos=$(echo "$pedir" | grep '\(RL\)\|\(AP\)\|\(HF\)')
  pedirNeg=$(echo "$pedir" | grep '\(LR\)\|\(PA\)\|\(FH\)')

  # collect data in lists
  listdataPos=$(echo "$imgs" | grep $pedirPos)
  listdataPos=$(echo $listdataPos | tr " " "@")
  listdataNeg=$(echo "$imgs" | grep $pedirNeg)
  listdataNeg=$(echo $listdataNeg | tr " " "@")

  # create a separate sub folder
  WD=$WD/topupeddy
  if [[ $overwrite -eq 1 ]]; then echo "Overwriting old directory"; [[ -d $WD ]] && rm -rf $WD; fi
  mkdir -p $WD
  echo "    working directory is $WD"
  mkdir -p $WD/rawdata
  mkdir -p $WD/topup
  mkdir -p $WD/eddy
  mkdir -p $WD/data
  mkdir -p $WD/logs

  # settings for topupeddy wrapper:
  #   echospacing should be in msecs
  #   pedircode: 1 for LR/RL, 2 for AP/PA
  #   GRAPPAfactor: In-plane parallel imaging factor (set to 1 for no GRAPPA)
  #   combinematchedflg: 2 for including in the ouput all volumes uncombined,
  #                      1 for including in the ouput and combine only volumes where both LR/RL (or AP/PA) pairs have been acquired,
  #                      0 for including (uncombined) single volumes as well
  # please see the scan protocol for these values
  echospacing=1.15
  [[ $subj == PEPPER ]] && echospacing=1.12
  GRAPPAfactor=2
  combinematchedflg=1

  # fancy queue selection (that fails if a broken node is still in the cluster)
<<"BLOCK_COMMENT"
  # randomly pick a relatively free node on jalapeno01-09
  # list all jobs running on jalapeno 01-09
  list=$(qstat -u \* | grep -o '@jalapeno0[0-9]' | cut -d'0' -f2)
  # count the number of jobs per node
  list=$(echo 1 2 3 4 5 6 7 8 9 $list | tr " " "\n" | sort -n | uniq -c | sort -n)
  # find the lowest number of users
  lowest=$(echo "$list" | head -1)
  lowest=$(echo $lowest | cut -d' ' -f1)
  # pick a random node from the ones with the fewest users
  nodes=($(echo "$list" | grep $lowest' [0-9]' | awk '{print $2}'))
  picknode=${nodes[$RANDOM % ${#nodes[@]}]}
  # assign queue
  topupqueue=long.q@jalapeno0$picknode.fmrib.ox.ac.uk
BLOCK_COMMENT
  # regular queue selection
  topupqueue=long.q

  # Check if topup files exist. If they do, skip
  if [[ -f $WD/topup/myfield.nii.gz ]]; then
    echo "    Topup output exists. Skipping topup"
    IDtopup=1
  else
    echo "    queueing data handling"
    IDinit=$(fsl_sub -q veryshort.q -N topupinit -l $WD/logs $scriptsdir/topup_initialise.sh $WD $listdataPos $listdataNeg $pedircode)

    echo "    queueing topup preprocessing"
    IDpreproc=$(fsl_sub -j $IDinit -q veryshort.q -N topuppreproc -l $WD/logs $scriptsdir/topup_preproc.sh $WD $echospacing $pedircode $b0dist $b0maxbval $GRAPPAfactor)

    echo "    queueing topup to $topupqueue"
    IDtopup=$(fsl_sub -j $IDpreproc -q $topupqueue -N runtopup -l $WD/logs $scriptsdir/run_topup.sh $WD/topup $flgBET)
    echo "    job ID topup: $IDtopup"
  fi

  echo "    queueing eddy"
  #IDeddy=$(fsl_sub -j ${IDtopup} -q long.q -s openmp,$MaxOPENMP_Nodes -N eddyopenmp -l $WD/logs $scriptsdir/run_eddy.sh $WD/eddy)
  IDeddy=$(fsl_sub -j ${IDtopup} -q $FSLGECUDAQ -N eddygpu -l $WD/logs $scriptsdir/run_eddy_gpu.sh $WD/eddy)

  echo "    queueing eddy postprocessing"
  IDpostproc=$(fsl_sub -j $IDeddy -q veryshort.q -N eddypost -l $WD/logs $scriptsdir/eddy_postproc.sh $WD $combinematchedflg $scriptsdir $b0maxbval $flgBET)

done
echo ""; echo "END: wrapper_topupeddy"
echo "$(date)"; echo ""
