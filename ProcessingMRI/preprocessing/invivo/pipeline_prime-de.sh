# Preprocessing in vivo data

# The processing is basically the same for the two datasets (last-scan/prime-de)

# This script relies on scripts from the MR comparative anatomy tooolbox MrCat
# availabe via https://github.com/neuroecology/MrCat

# AH Aug 2022

##########
# prime-de
##########

# This was acquired at the same time as the processed data already released on
# PRIME-DE, though the diffusion data is not provided through the website

# Note the fmri data released on prime-de is the raw ep2d data without any processing

# STRUCTURAL
############

# Do a first pass of preprocessing where we merge all data to generate reasonable
# brain mask. Then go back and correct each volume for bias field before
# running mcflirt

# Merge mprage images from #83-87 -> struct
folder=/vols/Data/BigMac/MRI/Invivo/prime-de/struct
mkdir -p $folder/raw
cd $folder/raw
for i in $(seq -f "%03g" 83 87); do ln -s ../../raw/*_001_${i}* ./; done
fslmerge -t struct *
fslmaths struct -Tmean struct

# Correct labels
file=struct_reorient.nii.gz
cp struct.nii.gz $file
fslorient -setqform 0.500000 0.000000 -0.000000 -64.000000 \
    0.000000 0.500000 -0.000000 -52.710094 \
    0.000000 0.000000 0.499950 14.593372 \
    0 0 0 1 $file
fslorient -copyqform2sform $file

# Move to first-pass folder
mkdir -p preprocessing/first-pass
mv struct_reorient.nii.gz preprocessing/first-pass/struct.nii.gz
rm struct*
cd preprocessing/first-pass

# Process using MrCat wrapper for bias field correction and masking
fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
    --subjdir=$PWD/ --structdir=$PWD/ \
    --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --all


# Perform bias field correction on each mprage image
cd $folder/raw
for i in {1..5}; do
    f=20100416_001_0$((i+82))_mprage_0_5x0_5x0_5mm.nii.gz
    file=mprage_0${i}_reorient.nii.gz
    cp $f $file
    fslorient -setqform 0.500000 0.000000 -0.000000 -64.000000 \
        0.000000 0.500000 -0.000000 -52.710094 \
        0.000000 0.000000 0.499950 14.593372 \
        0 0 0 1 $file
    fslorient -copyqform2sform $file
done


for i in {1..5}; do
    cd $folder/raw/
    mkdir -p preprocessing/mprage_0${i}
    cd preprocessing/mprage_0${i}
    ln -s ../../mprage_0${i}_reorient.nii.gz struct.nii.gz
    ln -s ../../preprocessing/first-pass/struct_brain_mask.nii.gz ./
    fslmaths struct.nii.gz -mul \
        struct_brain_mask.nii.gz struct_brain
    fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
        --subjdir=$PWD \
        --structdir=$PWD \
        --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --biascorr
done

# Coregister and average images. Use middle image as reference
# Use the omat 2->3 to initialise 1->3 and 4->3 to initalise 5->3
ref=mprage_03/struct_restore.nii.gz
cd $folder/raw/preprocessing
cp $ref mprage_03_reg.nii.gz
fsl_sub -q veryshort.q flirt -in mprage_02/struct_restore.nii.gz -ref $ref \
    -cost corratio -out mprage_02_reg -omat mprage_02.mat -interp spline -dof 6
fsl_sub -q veryshort.q flirt -in mprage_01/struct_restore.nii.gz -ref $ref \
    -cost corratio -out mprage_01_reg -init mprage_02.mat -interp spline -dof 6
fsl_sub -q veryshort.q flirt -in mprage_04/struct_restore.nii.gz -ref $ref \
    -cost corratio -out mprage_04_reg -omat mprage_04.mat -interp spline -dof 6
fsl_sub -q veryshort.q flirt -in mprage_05/struct_restore.nii.gz -ref $ref \
    -cost corratio -out mprage_05_reg -init mprage_04.mat -interp spline -dof 6

fslmerge -t mprage_merged mprage_??_reg*gz
fslmaths mprage_merged -Tmean struct_restore.nii.gz
ln -s struct_restore.nii.gz struct.nii.gz

# Proccess for final brain mask and registration to F99
fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
    --subjdir=$PWD/ --structdir=$PWD/ \
    --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --robustfov --betorig \
      --betrestore --register --brainmask --register --brainmask --segment

# Combine brain masks - resultant mask will be pretty generous
fslmaths struct_brain_mask_F99lin.nii.gz -add struct_brain_mask_bet.nii.gz -bin struct_brain_mask
fslmaths struct -mul struct_brain_mask struct_brain

# Copy files
cd $folder/
mkdir data
cd data
ln -s ../raw/preprocessing/struct_restore_brain.nii.gz data/struct_brain.nii.gz
ln -s ../raw/preprocessing/struct_restore.nii.gz data/struct.nii.gz
cd ../
mkdir masks
cd masks
ln -s ../raw/preprocessing/struct_brain_mask.nii.gz masks/brain_mask.nii.gz
cd ../
mkdir reg
mv raw/preprocessing/transform reg/struct_2_F99_flirt
mv raw/preprocessing/F99/struct_restore_lin.nii.gz reg/struct_2_F99_flirt/struct_2_F99_flirt.nii.gz
rm -r raw/preprocessing/F99


################################################################################

# DWI
#####

# Move dwi scans to own dwi folder.
folder=/vols/Data/BigMac/MRI/Invivo/prime-de/dwi
mkdir -p $folder/raw
cd $folder/raw
for i in ../../raw/*DTI*; do ln -s $i ./; done
rm *001_004*
for i in {19..26}; do rm *001_0${i}*; done

# Select datasets from #28 as data before appears incomplete or cut short
# Change naming of bvecs/bvals to .bvec and .bval
# Process using MrCat as above
# This script is provided in MrCat-dev
cd /home/fs0/amyh//scratch/MrCat-dev/pipelines/
./wrapper_topupeddy.sh /vols/Data/BigMac/Invivo/prime-de/dwi 1 0
cd /vols/Data/BigMac/Invivo/prime-de/dwi
mv topupeddy preprocessing
mv preprocessing raw

# Correct the labels
# Reorder x y z and make z,x positive
cd /vols/Data/BigMac/MRI/Invivo/prime-de/dwi/raw/preprocessing/data
for i in `find . -name "*gz"`; do
    fslorient -setqform 1.000000 0.000000 -0.000000 -49.299999 \
    0.000000 1.000000 -0.000000 -32.795200 \
    0.000000 0.000000 0.999899 21.400000 \
    0.000000 0.000000 0.000000 1.000000 $i;
    fslorient -copyqform2sform $i;
done
# Also correct bvecs
cp bvecs bvecsOrig
/home/fs0/amyh//func/bvecs_swap bvecsOrig x -y -z bvecs

# Extract S0 & create brain mask
/home/fs0/amyh//func/extract_shell.py -d data.nii.gz -b bvals -v bvecs -o b0k.nii.gz
fslmaths b0k -Tmean S0
rm b0k*

# Brain mask
# home/fs0/amyh//scratch/MrCat-dev/core/bet_macaque.sh S0.nii.gz -t T2star
# Misses some of the cerebellum
# Instead resample struct mask to 1mm using fsleyes (e.g. load structural mask > resample > resample to reference=S0)
fslmaths ../struct_brain_mask_resampled_1mm.nii.gz -thr 0.5 -bin ../struct_brain_mask_resampled_1mm_thr0.5
ln -s ../struct_brain_mask_resampled_1mm_thr0.5.nii.gz nodif_brain_mask.nii.gz

# Run dti to check
cd ../
mkdir data.dtifit
fsl_sub -q veryshort.q dtifit -k data/data.nii.gz -r data/bvecs -b data/bvals -m data/nodif_brain_mask.nii.gz -o data.dtifit/dti

# Move/copy files
cd ../../
mkdir data
cd data
for i in data.nii.gz bvals bvecs S0.nii.gz nodif_brain_mask.nii.gz; do
    ln -s ../raw/preprocessing/data/$i ./;
done
cd ../
mv raw/preprocessing/data.dtifit ./




################################################################################

# FUNCTIONAL
############
folder=/vols/Data/BigMac/MRI/Invivo/prime-de/func
mkdir -p $folder/raw
cd $folder/raw

# Copy data
for i in $(seq -f "%03g" 8 17); do ln -s ../../raw/*_001_${i}* ./; done

# Rename using soft links
ln -s 20100416_001_009_fieldmap_TEs_5_19_7_65.nii.gz fieldmap-mag.nii.gz
ln -s 20100416_001_010_fieldmap_TEs_5_19_7_65.nii.gz fieldmap-phase.nii.gz
ln -s 20100416_001_017_ep2d-fmri.nii.gz func.nii.gz

# Correct the labels
for i in func.nii.gz fieldmap*; do
    file=${i/.nii/_reorient.nii}
    cp $i $file
    fslorient -setqform 2.000000 0.000000 -0.000000 -62.265060 \
    0.000000 2.000000 -0.000000 -50.981552  \
    0.000000 0.000000 1.999800 5.072289 \
    0.000000 0.000000 0.000000 1.000000 $file;
    fslorient -copyqform2sform $file;
done

i=func_reorient.nii.gz
file=func_reorient2.nii.gz
cp $folder/$i $file
fslorient -setqform 2.000000 0.000000 -0.000000 0 \
    0.000000 2.000000 -0.000000 0  \
    0.000000 0.000000 1.999800 0 \
    0.000000 0.000000 0.000000 1.000000 $file;
fslorient -copyqform2sform $file;
# Note this is the same as the online data for primede, though one has neurological and the other radiological convention

# Resample structural mask into fmri space using fsleyes
# threshold
fslmaths struct_brain_mask_resampled_2mm.nii.gz -thr 0.5 -bin struct_brain_mask_resampled_2mm_thr0.5.nii.gz
ln -s struct_brain_mask_resampled_2mm_thr0.5.nii.gz brain_mask.nii.gz

# Copy data / files
cd $folder
mkdir data
mv data
ln -s ../raw/func.nii.gz ./
ln -s ../raw/brain_mask.nii.gz ./
cd $folder
mkdir masks
cd masks
ln -s ../raw/brain_mask.nii.gz ./

# Process using Saad's script
cd $folder/raw
mkdir preprocessing
cd preprocessing
ln -s ../func_reorient.nii.gz raw.nii.gz
ln -s ../../../struct/data/struct_brain.nii.gz ./
ln -s ../../../struct/data/struct.nii.gz ./
/home/fs0/amyh/scratch/MrCat-dev/monkeyRS-Saad/DO.sh $PWD
# This script is provided in MrCat-dev

# Clasify components
fsleyes -s melodic -ad func.ica
# Rerun
/home/fs0/amyh/scratch/MrCat-dev/monkeyRS-Saad/DO.sh $PWD

# Copy cleaned data to main folder
cd $folder/data
ln -s ../raw/preprocessing/func_filt.nii.gz func_cleaned.nii.gz
