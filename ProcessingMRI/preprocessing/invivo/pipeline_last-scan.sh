# Preprocessing in vivo data

# The processing is basically the same for the two datasets (last-scan/prime-de)

# This script relies on scripts from the MR comparative anatomy tooolbox MrCat
# availabe via https://github.com/neuroecology/MrCat

# AH Aug 2022

###########
# last-scan
###########

# This data is from the last in vivo session with the monkey. The data is
# therefore more comparable with BigMac as this data were acquired several
# months post surgery.

# This includes rs-fmri, dwi and structural images, all of which require
# processing

###########
# STRUCTURAL
############

# Do a first pass of preprocessing where we merge all data to generate reasonable
# brain mask. Then go back and correct each volume for bias field before
# running mcflirt

# Merge mprage images from #83-87 -> struct
folder=/vols/Data/BigMac/MRI/Invivo/last-scan/struct/raw
mkdir -p $folder
cd $folder
for i in ../../raw/*mprage*; do ln -s $i ./; done
fslmerge -t struct *
fslmaths struct -Tmean struct

# Correct labels
file=$folder/struct_reorient.nii.gz
cp $folder/struct.nii.gz $file
fslorient -setqform 0.500000 0.000000 -0.000000 -64.000000 \
    0.000000 0.500000 -0.000000 -68.956116 \
    0.000000 0.000000 0.499950 28.905880 \
    0 0 0 1 $file
fslorient -copyqform2sform $file

# Move to first-pass folder
mkdir -p preprocessing/first-pass
mv struct_reorient.nii.gz preprocessing/first-pass/struct.nii.gz
rm struct*
cd preprocessing/first-pass

# Process using MrCat wrapper for bias field correction and masking
fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
    --subjdir=$PWD/ --structdir=$PWD/ \
    --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --all

# Perform bias field correction on each mprage image
cd $folder/raw
for i in 1 2; do
    j=$((i+1))
    f=20170113_001_00$((j*2))_mprage_0_5x0_5x0_5mm.nii.gz
    file=mprage_0${i}_reorient.nii.gz
    cp $f $file
    fslorient -setqform 0.500000 0.000000 -0.000000 -64.000000 \
        0.000000 0.500000 -0.000000 -68.956116 \
        0.000000 0.000000 0.499950 28.905880 \
        0 0 0 1 $file
    fslorient -copyqform2sform $file
done

for i in 1 2; do
    mkdir -p preprocessing/mprage_0${i}
    ln -s $PWD/mprage_0${i}_reorient.nii.gz preprocessing/mprage_0${i}/struct.nii.gz
    ln -s $PWD/preprocessing/first-pass/struct_brain_mask.nii.gz preprocessing/mprage_0${i}/
    fslmaths preprocessing/mprage_0${i}/struct.nii.gz -mul \
        preprocessing/mprage_0${i}/struct_brain_mask.nii.gz \
        preprocessing/mprage_0${i}/struct_brain
    fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
        --subjdir=$PWD/preprocessing/mprage_0${i}/ \
        --structdir=$PWD/preprocessing/mprage_0${i}/ \
        --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --biascorr
done

# Coregister and average images. Use first image as reference
ref=mprage_01/struct_restore.nii.gz
cd $folder/raw/preprocessing
cp $ref mprage_01_reg.nii.gz
fsl_sub -q veryshort.q flirt -in mprage_02/struct_restore.nii.gz -ref $ref \
    -cost corratio -out mprage_02_reg -omat mprage_02.mat -interp spline -dof 6

fslmerge -t mprage_merged mprage_??_reg*gz
fslmaths mprage_merged -Tmean struct_restore.nii.gz
ln -s struct_restore.nii.gz struct.nii.gz

# Proccess for final brain mask and registration to F99
fsl_sub -q veryshort.q /home/fs0/amyh//scratch/MrCat-dev/core/struct_macaque.sh \
    --subjdir=$PWD/ --structdir=$PWD/ \
    --scriptdir=/home/fs0/amyh//scratch/MrCat-dev/core/ --robustfov --betorig \
      --betrestore --register --brainmask --register --brainmask --segment

# Combine brain masks - resultant mask will be pretty generous
fslmaths struct_brain_mask_F99lin.nii.gz -add struct_brain_mask_bet.nii.gz -bin struct_brain_mask
fslmaths struct -mul struct_brain_mask struct_brain

# Copy files
cd $folder/
mkdir data
cd data
ln -s ../raw/preprocessing/struct_restore_brain.nii.gz data/struct_brain.nii.gz
ln -s ../raw/preprocessing/struct_restore.nii.gz data/struct.nii.gz
cd ../
mkdir masks
cd masks
ln -s ../raw/preprocessing/struct_brain_mask.nii.gz masks/brain_mask.nii.gz
cd ../
mkdir reg
mv raw/preprocessing/transform reg/struct_2_F99_flirt
mv raw/preprocessing/F99/struct_restore_lin.nii.gz reg/struct_2_F99_flirt/struct_2_F99_flirt.nii.gz
rm -r raw/preprocessing/F99



################################################################################

# DWI
#####

# Separate dwi files
# Add bvec and bvals files. See bvecsbvals directory with bvecs/bvals for each
# direction set.
folder=/vols/Data/BigMac/MRI/Invivo/last-scan/dwi
mkdir -p $folder/raw
cd $folder/raw
for i in ../../raw/*DTI*; do ln -s $i ./; done
rm *001_013*
rm *001_015*
# Some files are excluded. These are files where the number of volumes do not
# match the desired number of gradient directions. i.e. the scan was cut short
# for some reason.
# Process dwi data using MrCat-dev wrapper script wrapper_topupeddy.sh
cd /home/fs0/amyh//scratch/MrCat-dev/pipelines/
./wrapper_topupeddy.sh $folder 1 0
# Note here I have made a couple of changes
# wrapper_topupeddy.sh now takes as input <dir> <flag> <overwrite>
# All B0s are considered, not only b0s 50 volumes apart as previously (b0dist=1)
# You can continue processing from a previous topupeddy output where topup has
# already ran (i.e. only processing eddy) by setting overwrite=0
# This edited script is provided in MrCat-dev
cd $folder
mv topupeddy preprocessing
mv preprocessing raw

# Correct the labels
# Reorder x y z and make z,x positive
for i in `find . -name "*gz"`; do fslorient -setqform \
  1.000000 0.000000 -0.000000 -56.555759 \
  0.000000 1.000000 -0.000000 -58.768635 \
  0.000000 0.000000 0.999901 34.699722 \
  0.000000 0.000000 0.000000 1.000000 $i; fslorient \
-copyqform2sform $i; done
# Also correct bvecs
cp bvecs bvecsOrig
/home/fs0/amyh//func/bvecs_swap bvecsOrig x -z -y bvecs

# Extract S0 & create brain mask
/home/fs0/amyh//func/extract_shell.py -d data.nii.gz -b bvals -v bvecs -o b0k.nii.gz
fslmaths b0k -Tmean S0
rm b0k*

# Brain mask
# home/fs0/amyh//scratch/MrCat-dev/core/bet_macaque.sh S0.nii.gz -t T2star
# Misses some of the cerebellum
# Instead resample struct mask to 1mm using fsleyes (e.g. load structural mask > resample > resample to reference=S0)
fslmaths ../struct_brain_mask_resampled_1mm.nii.gz -thr 0.5 -bin ../struct_brain_mask_resampled_1mm_thr0.5
ln -s ../struct_brain_mask_resampled_1mm_thr0.5.nii.gz nodif_brain_mask.nii.gz

# Run dti to check
cd ../
mkdir data.dtifit
fsl_sub -q veryshort.q dtifit -k data/data.nii.gz -r data/bvecs -b data/bvals -m data/nodif_brain_mask.nii.gz -o data.dtifit/dti

# Move/copy files
cd $folder
mkdir data
for i in data.nii.gz bvals bvecs S0.nii.gz nodif_brain_mask.nii.gz; do
    ln -s ./raw/preprocessing/data/$i data/;
done
mv raw/preprocessing/data.dtifit ./




################################################################################

# FUNCTIONAL
############
folder=/vols/Data/BigMac/MRI/Invivo/last-scan/func/
mkdir -p $folder/raw
cd $folder/raw

# Copy data
for i in $(seq -f "%03g" 3 14); do ln -s ../../raw/*_001_${i}* ./; done

# Rename using soft links
ln -s 20170113_001_010_fieldmap_TEs_5_19_7_65_4ChMonkey.nii.gz fieldmap-mag.nii.gz
ln -s 20170113_001_011_fieldmap_TEs_5_19_7_65_4ChMonkey.nii.gz fieldmap-phase.nii.gz
ln -s 20170113_001_007_ep2d-fmri_800volumes.nii.gz func.nii.gz

# Correct the labels
for i in func.nii.gz fieldmap*; do
    file=${i/.nii/_reorient.nii}
    cp $i $file
    fslorient -setqform 2.000000 0.000000 -0.000000 -66.168678 \
    0.000000 2.000000 -0.000000 -72.668297  \
    0.000000 0.000000 1.999800 26.084337 \
    0.000000 0.000000 0.000000 1.000000 $file;
    fslorient -copyqform2sform $file;
done


# Resample structural mask into fmri space using fsleyes
# threshold
fslmaths struct_brain_mask_resampled_2mm.nii.gz -thr 0.5 -bin struct_brain_mask_resampled_2mm_thr0.5.nii.gz
ln -s struct_brain_mask_resampled_2mm_thr0.5.nii.gz brain_mask.nii.gz

# Copy data / files
cd $folder
mkdir data
mv data
ln -s ../raw/func.nii.gz ./
ln -s ../raw/brain_mask.nii.gz ./
cd $folder
mkdir masks
cd masks
ln -s ../raw/brain_mask.nii.gz ./

# Process using Saad's script
cd $folder/raw
mkdir preprocessing
cd preprocessing
ln -s ../func_reorient.nii.gz func.nii.gz
ln -s ../../../struct/data/struct_brain.nii.gz ./
ln -s ../../../struct/data/struct.nii.gz ./
/home/fs0/amyh/scratch/MrCat-dev/monkeyRS-Saad/DO.sh $PWD
# This script is provided in MrCat-dev

# Clasify components
fsleyes -s melodic -ad func.ica
# Only one component is classified as signal!
# Re-run
/home/fs0/amyh/scratch/MrCat-dev/monkeyRS-Saad/DO.sh $PWD

# Copy cleaned data to main folder
cd $folder/data
ln -s ../raw/preprocessing/func_filt.nii.gz func_cleaned.nii.gz
