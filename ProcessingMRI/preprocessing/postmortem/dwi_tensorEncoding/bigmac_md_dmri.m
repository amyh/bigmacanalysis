%% Pipeline to process tensor encoded BigMac data
% AH Jan 2022

% Run pipeline according to 'multi-dimensional dmri' toolbox 
% available from 
% https://github.com/markus-nilsson/md-dmri/tree/master/methods
% Nilsson et al.

% Here we follow the DIVIDE processing to calculate various properties of
% the micro-tensors
% https://doi.org/10.1371/journal.pone.0214238
% Szczepankiewicz et al 2019

% This paper provides a very good explanation of the analysis background
% https://doi.org/10.3389/fphy.2014.00011
% Lasic et al 2014

% Micro FA - fractional anisotropy of the microenvironment, indep of
% dispersion
% Isotropic mean kurtosis - describes the variance of diffusion
% coefficients associated with isotropic diffusion. e.g. if the tissue
% includes many isotropic compartments of different sizes, MKi will be
% large
% Anisotropic mean kurtosis - similar but now for anisotropic diffusion 
% OP = order parameter used to describe orientation. This is calculated by
% comparing FA and microFA

clear all, close all
cd /Users/amyh/Desktop/func/md-dmri

setup_paths()
ddir = '/Users/amyh/Desktop/tmp/tensorEncoding/';

% Load data to md-dmri file format
s{1} = data2xps([ddir 'b04k/1.0mm/data.LTE/'],1);
s{2} = data2xps([ddir 'b04k/1.0mm/data.STE/'],0);
s{3} = data2xps([ddir 'b07k/1.0mm/data.LTE/'],1);
s{4} = data2xps([ddir 'b07k/1.0mm/data.STE/'],0);
s{5} = data2xps([ddir 'b10k/1.0mm/data.LTE/'],1);
s{6} = data2xps([ddir 'b10k/1.0mm/data.STE/'],0);

%% 
% Merge info from all data
% Define a name for the merged nii (output)
merged_nii_path = [ddir 'data.merged/'];
merged_nii_name = 'data';

% Merge the s structure, and save the merged nii along with its corresponding xps.mat file.
s_merged = mdm_s_merge(s, merged_nii_path, merged_nii_name);
system(['cp ' ddir 'b04k/1.0mm/masks/nodif_brain_mask.nii.gz ' merged_nii_path]);

%%
%%% Run model %%%
% data: [merged_nii_path merged_nii_name] 
% mask: [merged_nii_path '/nodif_brain_mask.nii.gz'] 
% method: 'dtd_gamma' 
% out: [merged_nii_path '/dtd_divide'] 
% xps: [merged_nii_path '/data_xps.mat'] 
% 

mdm_fit --data /Users/amyh/Desktop/tmp/tensorEncoding/data.merged/data.nii.gz ...
    --mask /Users/amyh/Desktop/tmp/tensorEncoding/data.merged//nodif_brain_mask.nii.gz ...
    --method dtd_gamma ...
    --out /Users/amyh/Desktop/tmp/tensorEncoding/data.merged// ...
    --xps /Users/amyh/Desktop/tmp/tensorEncoding/data.merged//data_xps.mat;

disp('done')

%%

function s = data2xps(folder,b_delta)

% Path to data
s.nii_fn = [folder '/data.nii.gz'];

% Get path to corresponding bval/bvec, and generate the xps
bval_fn = [folder '/bvals'];
bvec_fn = [folder '/bvecs'];
s.xps     = mdm_xps_from_bval_bvec(bval_fn, bvec_fn, b_delta);

% Finally, save the xps.
xps_fn    = mdm_xps_fn_from_nii_fn(s.nii_fn);
mdm_xps_save(s.xps, xps_fn)
end

