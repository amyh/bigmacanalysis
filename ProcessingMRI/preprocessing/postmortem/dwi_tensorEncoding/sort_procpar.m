%% Take bvals and bvecs from scanner and print to txt file

% AH Jan 2022
%close all, clear all
ddir = '/vols/Data/BigMac/MRI/Postmortem/dwi/TensorEncoding/raw/';

for b = 1:6
   
if b==1
    outdir = [ddir '/b04k/linear/'];
    files = {'NS1_AK5_G_3327_70_semsdw_1k_029.nifti/',...
        'NS1_AK5_G_3327_70_semsdw_1k_031.nifti'};
    enc = {'LTE','LTE'};
    bv = 4;

elseif b==2
    outdir = [ddir '/b04k/spherical/'];
    files = {'NS1_AK5_G_3327_70_sems_iso_059.nifti/',...
        'NS1_AK5_G_3327_70_sems_iso_061.nifti',...
        'NS1_AK5_G_3327_70_sems_iso_064.nifti',...
        'NS1_AK5_G_3327_70_semsdw_1k_066.nifti'};
    enc = {'STE','STE','STE','LTE'};
    bv = 4;

elseif b==3
    outdir = [ddir '/b07k/linear/'];
    files = {'NS1_AK5_G_3327_70_semsdw_1k_039.nifti/',...
        'NS1_AK5_G_3327_70_semsdw_1k_041.nifti'};
    enc = {'LTE','LTE'};
    bv = 7;

elseif b==4
    outdir = [ddir '/b07k/spherical/'];
    files = {'NS1_AK5_G_3327_70_semsdw_1k_043.nifti/',...
        'NS1_AK5_G_3327_70_sems_iso_045.nifti'};
    enc = {'LTE','STE'};
    bv = 7;

elseif b==5
    outdir = [ddir '/b10k/linear/'];
    files = {'NS1_AK5_G_3327_70_semsdw_1k_049.nifti/',...
        'NS1_AK5_G_3327_70_semsdw_1k_051.nifti'};
    enc = {'LTE','LTE'};
    bv = 10;

elseif b==6
    outdir = [ddir '/b10k/spherical/'];
    files = {'NS1_AK5_G_3327_70_semsdw_1k_053.nifti/',...
        'NS1_AK5_G_3327_70_sems_iso_055.nifti'};
    enc = {'LTE','STE'};
    bv = 10;

end


%%
bvals = [];
bvecs = [];

for i = 1:numel(files)
    file = fopen([ddir files{i} '/procpar'],'r');
    inf = niftiinfo([ddir files{i} '/MG/image001.nii']);
    if numel(inf.ImageSize)==3
        nvol = 1;
    else
        nvol = inf.ImageSize(4);
    end
    % For linear encoding, extract bvals and bvecs from procpar
    if strcmp(enc{i},'LTE')
        formatspec = '%s';
        A = textscan(file,formatspec,'Delimiter',' ');
        fclose(file);
        a = string([A{:}]);
        % Find info about bvalues
        ind = find(a == 'bvalue');
        % They begin 12 values after
        ind = ind+12;
        % Collect bvalues
        bs = a(ind:ind+nvol-1);
        bvals = [bvals;str2double(bs)];

        % Repeat for bvecs
        for v = ["dro","dpe","dsl"]
            ind = find(a == v);
            % They begin 12 vaes after
            ind = ind+12;
            % Collect values
            subset = a(ind:ind+nvol-1);
            if v == "dro", dro = str2double(subset); end
            if v == "dpe", dpe = str2double(subset); end
            if v == "dsl", dsl = str2double(subset); end          
        end
        
        tmp = [dpe,dsl,dro];
        bvecs = [bvecs; tmp];
        
    else
        % Bvals and bvecs not included in procpar of spherical data
        % Set bvals to desired value, and bvecs to arbitrary [0 0 0]
        bvals = [bvals; bv*1e3*ones(nvol,1)];
        tmp = zeros(nvol,3);
        %tmp(:,1) = 1;
        bvecs = [bvecs; tmp];
    end
        
end

%bvecs = [dro,dpe,dsl];

% Reorient x y z according to the reorientation of the data
% This was previously found to be



%%
% Write to text file
fbval = [outdir '/bvals.T'];
fbvec = [outdir '/bvecs.T'];
f1 = fopen(fbval,'w');
f2 = fopen(fbvec,'w');
bvalspec = '%d \n';
bvecspec = '%f %f %f \n';
for i = 1:size(bvals)
    fprintf(f1,bvalspec,bvals(i));
    fprintf(f2,bvecspec,[bvecs(i,1),bvecs(i,2),bvecs(i,3)]);
end
fclose(f1);
fclose(f2);
system(['~/func/transpose_bvecs.sh ' fbval ' > ' fbval(1:end-2)]);
system(['~/func/transpose_bvecs.sh ' fbvec ' > ' fbvec(1:end-2)]);

end
