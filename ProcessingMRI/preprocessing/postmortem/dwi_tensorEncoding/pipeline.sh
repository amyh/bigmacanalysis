# Processing b04, b07k & b10k data
# AH Jan 2022

# Note, b04k spherical data was messed up on first attempt. Consequently data
# were acauired at the end of the scanning sesion. As tbere was not time to wait
# for the scanner to cool after the b10k data, we may not trust the first part
# of b04k spherical scans. Consequently, may want to discard some of the early
# volumes, as we have 30 repeats.

p=/vols/Data/BigMac/MRI/Postmortem/scripts/preprocessing/dwi_tensorEncoding/
ddir=/vols/Data/BigMac/MRI/Postmortem/dwi/TensorEncoding/
cd $ddir/raw

# b04k linear
fslmerge -t b04k/LTE/data \
    NS1_AK5_G_3327_70_semsdw_1k_029.nifti/MG/image001.nii  \
    NS1_AK5_G_3327_70_semsdw_1k_031.nifti/MG/image001.nii  \

# b04k spherical
# Note, b04k spherical data was acquired straight after the b10k data, without there being time for the scanner to cool down. Consequently, may be best to only use latter part of data (total imaging time ~4hrs) where scanner will have somewhat cooled.
fslmerge -t b04k/STE/data \
    NS1_AK5_G_3327_70_sems_iso_059.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_sems_iso_061.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_sems_iso_064.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_semsdw_1k_066.nifti/MG/image001.nii

# b07k linear
fslmerge -t b07k/LTE/data \
    NS1_AK5_G_3327_70_semsdw_1k_039.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_semsdw_1k_041.nifti/MG/image001.nii \

# b07k spherical
fslmerge -t b07k/STE/data \
    NS1_AK5_G_3327_70_semsdw_1k_043.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_sems_iso_045.nifti/MG/image001.nii

# b10k linear
fslmerge -t b10k/LTE/data \
    NS1_AK5_G_3327_70_semsdw_1k_049.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_semsdw_1k_051.nifti/MG/image001.nii \

# b10k spherical
fslmerge -t b10k/STE/data \
    NS1_AK5_G_3327_70_semsdw_1k_053.nifti/MG/image001.nii \
    NS1_AK5_G_3327_70_sems_iso_055.nifti/MG/image001.nii

# Reorient and deGibbs data
module add mrdegibbs3d
for b in 04 07 10; do
    for e in LTE STE; do
        file=b${b}k/$e/data.nii.gz
        cp $file ${file/data/tmp}
        fslorient -deleteorient $file
        fslswapdim $file -y z -x $file
        fslcpgeom ${file/image/tmp} $file -d
        fslorient -setqform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 $file
        fslorient -copyqform2sform $file
        rm ${file/data/tmp}
        deGibbs3D -force $file ${file/data/data_gibbsCorr}
    done
done

# Extract bvecs and bvals from propcar files
# Reorient if necc to ensure conistent with the data
$p/sort_procpar.m

# Check orientations
cd b04k/LTE
# Create brain mask
fslroi data_gibbsCorr.nii.gz nodif_brain_mask 0 -1 0 -1 0 -1 0 1
fslmaths nodif_brain_mask.nii.gz -thr 1000 -bin -fillh nodif_brain_mask.nii.gz
# run dtifit
dtifit -k data_gibbsCorr.nii.gz -o dtifit/dti -r bvecs -b bvals -m nodif_brain_mask.nii.gz
# Orientations look good
cd ../../
mkdir masks
cp b04k/LTE/nodif_brain_mask.nii.gz masks/

# Extract b0's from each file to see if there is any displacement between images
for b in 04 07 10; do
    for e in LTE STE; do
        folder=b${b}k/$e/
        ~/func/extract_shell.py -d $folder/data_gibbsCorr.nii.gz -b $folder/bvals -v $folder/bvecs -s 0 -o $folder/S0_gibbsCorr.nii.gz
    done
done
fslmerge -t all_S0 b04k/LTE/S0_gibbsCorr b04k/STE/S0_gibbsCorr b07k/LTE/S0_gibbsCorr b07k/STE/S0_gibbsCorr b10k/LTE/S0_gibbsCorr b10k/STE/S0_gibbsCorr
# No clear displacement across scans => no reason to register

# Check signal stability across volumes
# Particularly important in b04k STE where scanner was cooling
fslmaths masks/nodif_brain_mask.nii.gz -dilM -bin masks/nodif_brain_mask_dilM
for b in 04 07 10; do
    cd b${b}k/STE/
    mkdir split
    fslsplit data_gibbsCorr.nii.gz split/ -t
    rm mean_sig_per_vol
    for i in split/*; do fslstats $i -k ../../masks/nodif_brain_mask_dilM -M >> mean_sig_per_vol; done
    rm -r split
    cd ../../
done
# Plot in matlab
# Have increasing signal over time
# Detrend data - linear approximation
for b in 04 07 10; do
    for e in STE; do
        folder=b${b}k/$e
        data=data_gibbsCorr
        bvals=bvals
        # Cannot fit linear model to b=0 data as there is only a single bvalue => separate data befor processing
        if [[ $e == STE ]]; then
            ~/func/extract_shell.py -d $folder/data_gibbsCorr.nii.gz -b $folder/bvals -v $folder/bvecs -s ${b}000 -o $folder/b${b}k_gibbsCorr.nii.gz
            data=b${b}k_gibbsCorr
            bvals=${data}.bvals
        fi
        $p/../detrend_dwi.py -d $folder/${data}.nii.gz -b $folder/$bvals -o $folder/${data}_detrend.nii.gz

        # Scale data to signal most similar to b0
        if [[ $e == STE ]]; then
            mkdir $folder/split
            fslsplit $folder/${data}.nii.gz $folder/split/ -t
            if [[ $b == 04 ]]; then
                N=0029
            else
                N=0000
            fi
            Mnew=`fslstats $folder/split/${N}.nii.gz -k masks/nodif_brain_mask_dilM -M`
            Mold=`fslstats $folder/${data}_detrend.nii.gz -k masks/nodif_brain_mask_dilM -M`
            cp  $folder/${data}_detrend.nii.gz  $folder/${data}_detrend_old.nii.gz
            echo $Mold $Mnew
            fslmaths $folder/${data}_detrend.nii.gz -div $Mold -mul $Mnew $folder/${data}_detrend.nii.gz
            rm -r $folder/split

            # Merge data back together
            if [[ $b == 04 ]]; then
                fslmerge -t $folder/data_gibbsCorr_detrend $folder/${data}_detrend.nii.gz $folder/S0_gibbsCorr.nii.gz
            else
                fslmerge -t $folder/data_gibbsCorr_detrend $folder/S0_gibbsCorr.nii.gz $folder/${data}_detrend.nii.gz
            fi
            #rm $folder/b${b}k_gibbsCorr*
        fi

    done
done
# Compare mean sig to previous values
for b in 04 07 10; do
    cd b${b}k/STE/
    mkdir split
    fslsplit data_gibbsCorr_detrend.nii.gz split/ -t
    rm mean_sig_per_vol_detrend
    for i in split/*; do fslstats $i -k ../../masks/nodif_brain_mask_dilM -M >> mean_sig_per_vol_detrend; done
    rm -r split
    cd ../../
done

# Normalise b0s
for b in 04 07 10; do
    for e in LTE STE; do
        folder=b${b}k/$e
        $p/../equate_b0s.py -d ${folder}/data_gibbsCorr_detrend.nii.gz \
         -m masks/nodif_brain_mask.nii.gz -b ${folder}/bvals \
         -r b10k/LTE/data_gibbsCorr_detrend.nii.gz -rm masks/nodif_brain_mask.nii.gz \
         -rb b10k/LTE/bvals -o ${folder}/data_gibbsCorr_detrend_norm.nii.gz
     done
done

# Copy data to main folder
cd $ddir
for b in 04 07 10; do
    for e in LTE STE; do
        f1=raw/b${b}k/$e/
        f2=b${b}k/1.0mm/data.${e}/
        m=b${b}k/1.0mm/masks
        mkdir -p $f2
        unlink $f2/data.nii.gz
        unlink $f2/bvals
        unlink $f2/bvecs
        unlink $f2/nodif_brain_mask.nii.gz
        ln -s $ddir/$f1/data_gibbsCorr_detrend_norm.nii.gz $f2/data.nii.gz
        ln -s $ddir/$f1/bvals $f2/
        ln -s $ddir/$f1/bvecs $f2/
        mkdir -p $m
        unlink $m/nodif_brain_mask.nii.gz
        ln -s $ddir/raw/masks/nodif_brain_mask.nii.gz $m/
        ln -s ../masks/nodif_brain_mask.nii.gz $f2/
    done
    unlink b${b}k/1.0mm/raw
    ln -s $ddir/raw b${b}k/1.0mm/raw
done

# Fit dti model to LTE data
for b in 04 07 10; do
    e=LTE
    cd b${b}k/1.0mm/
    mkdir data.LTE.dtifit
    dtifit -k data.LTE/data -b data.LTE/bvals -r data.LTE/bvecs -m data.LTE/nodif_brain_mask -o data.LTE.dtifit/dti;
    cd ../../
done


mkdir -p b04k/1.0mm/data.STE
mkdir -p b07k/1.0mm/data.LTE
mkdir -p b07k/1.0mm/data.STE
mkdir -p b10k/1.0mm/data.LTE
mkdir -p b10k/1.0mm/data.STE

# Process using md-dmri toolbox by Nilsson etc to extract microFA, MKi/a
# toolbox is matlab based
# see bigmac_md_dmri.m

# Could see striping (DC?) artefact - interestingly in different positions in LTE/STE data
# Create 'artefact' mask from mean intensity projection (across volumes) of data
fslmaths data.nii.gz -Tmax data_Tmax
# Mask by hand obvious artfeact
# save masks/artefact_stripes
