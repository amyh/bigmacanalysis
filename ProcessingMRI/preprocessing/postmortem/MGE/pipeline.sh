# Pipeline to process structural (MGE) data
folder=/vols/Data/BigMac/MRI/Postmortem/struct/MGE/raw/NS1_AK5_G_3327_70_sems/

fslchfiletype NIFTI_GZ /vols/Data/BigMac/MRI/Postmortem/.raw/stdProtocol/NS1_AK5_G_3327_70_mge3d_SOS_042.nifti/MG/sqrtsos001.nii $folder/struct.nii.gz

file=$folder/struct_reorient.nii.gz
cp $folder/struct.nii.gz $file
#fslreorient2std $file $file
fslorient -deleteorient $file
fslswapdim $file z -x -y $file
fslcpgeom $folder/struct.nii.gz $file -d
fslorient -setqform 0.3 0 0 0 0 0.3 0 0 0 0 0.3 0 0 0 0 1 $file
fslorient -copyqform2sform $file

# Correct gibbs ringing
../gibbs_corr.sh $folder struct_reorient
#module add mrdegibbs3d
#deGibbs3D -force $file $folder/struct_reorient_gibbsCorr.nii.gz

# Tissue mask
# Threshold image & edit by hand
mkdir $folder/preprocessing
ln -s $folder/struct_reorient_gibbsCorr.nii.gz $folder/preprocessing/struct.nii.gz
fslmaths $folder/preprocessing/struct -thr 200000 -bin $folder/preprocessing/struct_brain_mask
# edit by hand to fill holes
fslmaths $folder/preprocessing/struct -mul $folder/preprocessing/struct_brain_mask $folder/preprocessing/struct_brain


# Process data using MrCat
/Volumes/BigMac/MrCat/MrCat-win/core/struct_macaque.sh --biascorr --register --brainmask --biascorr --register --brainmask --segment --hemimask --structdir=$folder/preprocessing --structimg=$folder/preprocessing/struct.nii.gz --structmask=./struct_brain_mask.nii.gz --refimg=/Volumes/BigMac/MrCat/MrCat-win/data/macaque/F99/F99_restore.nii.gz --refmask=/Volumes/BigMac/MrCat/MrCat-win/data/macaque/F99/F99_brain_mask.nii.gz --config=/Volumes/BigMac/MrCat/MrCat-win/config/fnirt_1mm.cnf
# - bias field correction
# - brain masking
# - registration to F99 standard space
# - tissue segmentation
# - mask hemispheres

# Note, only keep bias field corrected images.
cp $folder/preprocessing/struct_bias* $folder/
cp $folder/preprocessing/struct_brain_mask_F99lin.nii.gz $folder/
rm -r $folder/preprocessing
fslmaths $folder/struct_bias_restore -mul $folder/struct_brain_mask $folder/struct_bias_restore_brain

# Copy to data folder
data=/vols/Data/BigMac/MRI/Postmortem/struct/MGE/data
mkdir $data
cd $data
ln -s ../raw/NS1_AK5_G_3327_70_sems/struct_bias_restore.nii.gz ./struct.nii.gz
ln -s ../raw/NS1_AK5_G_3327_70_sems/struct_bias_restore_brain.nii.gz ./struct_brain.nii.gz

# Copy to masks folder
masks=/vols/Data/BigMac/MRI/Postmortem/struct/MGE/masks
mkdir $masks
cd $masks
ln -s ../raw/NS1_AK5_G_3327_70_sems/struct_brain_mask_F99lin.nii.gz ./brain_mask_F99lin.nii.gz
ln -s ../raw/NS1_AK5_G_3327_70_sems/struct_brain_mask.nii.gz ./brain_mask_rough.nii.gz
ln -s ../raw/NS1_AK5_G_3327_70_sems/struct_brain_mask_unfilled.nii.gz ./brain_mask_unfilled.nii.gz
fslmaths ./brain_mask_unfilled.nii.gz -fillh ./brain_mask_filled.nii.gz
ln -s ./brain_mask_filled.nii.gz ./brain_mask.nii.gz

# Run fast to segment tissue
mkdir $data/fast
fast -t 2 -n 2 -g -N -P -A /vols/Data/BigMac/MRI/Standard/F99/data/McLaren_GM.nii.gz /vols/Data/BigMac/MRI/Standard/F99/data/McLaren_WM.nii.gz /vols/Data/BigMac/MRI/Standard/F99/data/McLaren_CSF.nii.gz -o $data/fast/struct_bias_restore_brain $data/struct_bias_restore_brain
# Note, does not provide very reliable wm segmentation
# HAND EDIT
# Masks have been created using a combination of the output above, thresholding,
# fast segementation, and considerable hand editing, the details of which are omitted.
