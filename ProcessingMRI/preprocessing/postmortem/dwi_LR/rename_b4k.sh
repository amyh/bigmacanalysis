#!/bin/sh
set -e
set -o errexit
# Rename files im b4k to have same numbering as b7k and b10k
# Easier to process together

file=$1

if [ ${file##*/} == 70_20180420_SEMSDW_Macaque250.nifti ]; then
    mv ${file} ${file##*/}70_20180420_SEMSDW_B4k.nifti


    cd ${file##*/}70_20180420_SEMSDW_B4k.nifti

    for ((i=5;i<26;i=i+2)); do
        j=$((i - 2))
        if [ $i -lt 10 ]; then
            mv NS1_*00$i.nifti NS1_AK5_G_3327_70_semsdw_1k_00$j.nifti
        elif [ $i -gt 10 ]; then
            mv NS1_*0$i.nifti NS1_AK5_G_3327_70_semsdw_1k_0$j.nifti
        fi
    done

    mv NS*09.nifti NS1_AK5_G_3327_70_semsdw_1k_009.nifti

fi
