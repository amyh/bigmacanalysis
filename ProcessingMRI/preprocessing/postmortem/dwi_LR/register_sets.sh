#!/bin/bash

# Register images together
# Divide b0 images into 'sets' which each have the same central frequency
# Take mean b0 from each set
# Register all mean set images from b10k and b7k using FLIRT
# $cost cost and spline interpolation
# restrict to 2 dof - translation along yz only
# Once transform for each set is found, run a mcflirt type optimisation.
# this allows us to account for small translations likely due to signal drift
# - take ~ middle image from each set and use matrix for set to initialise
# flirt optimisation
# - then take the next image, and use previous image to initialise
# - similarly, worrk backwards from middle image to start of set
# after transformation is calculated for each bvalue, apply to dwi vols
# use b10k 10th b0 as reference image


# Can acquire scanner central frequency from procpar files. see excel sheet

# AH 12/10/2018 - edited Sep 2021

echo "Beginning FLIRT registration."

bval=$1
folder=$2

cd $folder

N=40
M=1000

rm -r registration
mkdir ./registration/
mkdir ./registration/temp/
mkdir ./registration/sets/
mkdir ./registration/matrices/
mkdir ./registration/b0images

cd registration

# Register together masked b0's

# Split into experiments (ie 1 b0 followed by 25 dir)
echo "Splitting images."
fslsplit ../data_gibbsCorr.nii.gz ./temp/

# Extract b0 images
for ((i=0;i<$(( M + N ));i=i+26)); do
    filename=`printf '%04u' $i`
    cp ./temp/$filename.nii.gz ./b0images/
done

# Change to sensible numbering
fslmerge -t ./temp.nii.gz ./b0images/*
rm ./b0images/*
fslsplit ./temp.nii.gz ./b0images/
rm ./temp.nii.gz

# Pick reference image for registration - 10th b0 image
ref=/vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/raw/70_20180329_SEMSDW_B10k.nifti/registration/ref.nii.gz

# Use intra-modal cost function
cost=normcorr

if [ $bval == "10" ]; then
    # copy reference image
    cp ./b0images/0008.nii.gz ./ref.nii.gz
fi

echo Splitting images into sets
if [ $bval == "10" ]; then

    # Extract b0's which belong to each 'set' of central frequency values - ie 4 sets for b10k
    cp ./b0images/0000* ./sets/set1.nii.gz
    for ((i=1;i<19;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set1 ./sets/set1 ./b0images/$filename
    done

    cp ./b0images/0019* ./sets/set2.nii.gz
    for ((i=20;i<30;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set2 ./sets/set2 ./b0images/$filename
    done

    cp ./b0images/0030* ./sets/set3.nii.gz
    fslmerge -t ./sets/set3 ./sets/set3 ./b0images/0031
    for ((i=33;i<40;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set3 ./sets/set3 ./b0images/$filename
    done

    cp ./b0images/0032* ./sets/set4.nii.gz

    # Take mean image across time series
    for ((i=1;i<=3;i++)); do
        fslmaths ./sets/set$i -Tmean ./sets/set$i
    done

    # Mask images prior to registration
    for ((i=1;i<=4;i++)); do
        fslmaths ./sets/set$i -thr 1500 -bin ./sets/set${i}mask
        fslmaths ./sets/set${i}mask -fillh ./sets/set${i}mask
        fslmaths ./sets/set$i -mul ./sets/set${i}mask ./sets/set$i
    done

    fslmaths $ref -mul ./sets/set1mask $ref

    # Register images
    echo Beginning registration
    for ((i=2;i<=4;i++)); do
        # Find transform matrix
        flirt -in ./sets/set$i -cost $cost -nosearch -ref ./sets/set1  -omat ./matrices/set$i -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./sets/set${i}reg
        # spline interpolation gives more even contrast between the reference and registered images
    done

    ln -s ./sets/set1 ./sets/set1reg

    ################################# MCFLIRT ##################################
    # Now run mcflirt style registration, to account for signal drift

    # Set 1: 0-18 - begin either side of reference image
    for ((i=8;i<19;i++)); do
        filename=`printf '%04u' $i`
        # mask prior to registration
        fslmaths ./b0images/$filename -mul ./sets/set1mask ./b0images/$filename
        if [ $i == "8" ];  then
            printf "1  0  0  0 \n0  1  0  0  \n0  0  1  0  \n0  0  0  1  \n" > ./matrices/mat$filename
            cp ./b0images/${filename}.nii.gz ./b0images/${filename}reg.nii.gz
        else
            # Find transform matrix
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
            # spline interpolation gives more even contrast between the reference and registered images
        fi
    done

    for ((i=8;i>=0;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set1mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

    # Set 2: 19-29
    for ((i=25;i<30;i++)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set2mask ./b0images/$filename
        if [ $i == "25" ]; then
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/set2
        else
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
        fi
    done

    for ((i=24;i>=19;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set2mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

    # set 3 : 30, 31, 33-39
    for ((i=35;i<40;i++)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set3mask ./b0images/$filename
        if [ $i == "35" ]; then
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/set3
        else
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
        fi
    done

    for ((i=34;i>=30;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set3mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

    # only one image in set4: 32
    i=32
    filename=`printf '%04u' $i`
    fslmaths ./b0images/$filename -mul ./sets/set4mask ./b0images/$filename
    flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./b0images/${filename}reg -init ./matrices/set4

fi

################################################################################
################################################################################

if [ $bval == "07" ]; then

    # Extract b0's which belong to each 'set' of central frequency values - ie 3 sets for b7k
    cp ./b0images/0000* ./sets/set1.nii.gz
    for ((i=1;i<20;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set1 ./sets/set1 ./b0images/$filename
    done

    cp ./b0images/0020* ./sets/set2.nii.gz
    for ((i=21;i<33;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set2 ./sets/set2 ./b0images/$filename
    done

    cp ./b0images/0033* ./sets/set3.nii.gz
    for ((i=34;i<40;i=i+1)); do
        filename=`printf '%04u' $i`
        fslmerge -t ./sets/set3 ./sets/set3 ./b0images/$filename
    done

    # Take mean image across time series
    for ((i=1;i<=3;i++)); do
        fslmaths ./sets/set$i -Tmean ./sets/set$i
    done

    # Mask images prior to registration
    for ((i=1;i<=3;i++)); do
        fslmaths ./sets/set$i -thr 1200 -bin ./sets/set${i}mask
        fslmaths ./sets/set${i}mask -fillh ./sets/set${i}mask
        fslmaths ./sets/set$i -mul ./sets/set${i}mask ./sets/set$i
    done

    # Register images
    echo Beginning registration
    for ((i=1;i<=3;i++)); do
        # Find transform matrix
        #flirt -in ./sets/set$i -cost $cost -nosearch -ref $ref  -omat ./matrices/set$i -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_2dof_yz -interp spline -out ./sets/set${i}reg
        flirt -in ./sets/set$i -cost $cost -nosearch -ref $ref  -omat ./matrices/set$i -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./sets/set${i}reg
    done

    ################################# MCFLIRT ##################################
    # Now run mcflirt style registration, to account for signal drift

    # set 1: 0-19
    for ((i=10;i<20;i++)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set1mask ./b0images/$filename
        if [ $i == "10" ]; then
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/set1
        else
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
        fi
    done

    for ((i=9;i>=0;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set1mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

    # set 2: 20-32
    for ((i=26;i<33;i++)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set2mask ./b0images/$filename
        if [ $i == "26" ]; then
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/set2
        else
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
        fi
    done

    for ((i=25;i>=20;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set2mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

    # set 3: 33-39
    for ((i=36;i<40;i++)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set3mask ./b0images/$filename
        if [ $i == "36" ]; then
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/set3
        else
            j=`printf '%04u' $(( $i - 1 ))`
            flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
        fi
    done

    for ((i=35;i>=33;i--)); do
        filename=`printf '%04u' $i`
        fslmaths ./b0images/$filename -mul ./sets/set3mask ./b0images/$filename
        j=`printf '%04u' $(( i + 1 ))`
        flirt -in ./b0images/$filename -cost $cost -nosearch -ref $ref  -omat ./matrices/mat$filename -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/sch3Dtrans_3dof -interp spline -out ./b0images/${filename}reg -init ./matrices/mat$j
    done

fi

# Apply regsitration to all images
mkdir ./reg
for ((i=0;i<1040;i++)); do
    j=$(( i / 26 ))
    j=`printf '%04u' $j`
    filename=`printf '%04u' $i`
    flirt -in ./temp/$filename -ref $ref -applyxfm -interp spline -init ./matrices/mat$j -out ./reg/$filename
    rm ./temp/$filename.*
done

# Merge files
fslmerge -t ./b0k ./b0images/*{0..9}.nii.gz
fslmerge -t ./b0kreg ./b0images/*reg.nii.gz
fslmerge -t ../data_gibbsCorr_reg ./reg/*

# Remove folder - should now be empty - sanity check
rm -r ./temp
rm -r ./b0images
rm -r ./sets
rm -r ./reg

echo "Completed data registration"
