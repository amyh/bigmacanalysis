#!/bin/bash

# AH Apr 2018 - edited Sep 2021
# Suggested pipeline to process b04/b07/b10k 1mm diffusion images
p=/vols/Data/BigMac/MRI/Postmortem/scripts/preprocessing/dwi_LR

for bval in 10 07 04; do
    f=/vols/Data/BigMac/MRI/Postmortem/dwi/b${bval}k/1.0mm/raw/

    if [ $bval == "04" ]; then
        folder=$f/70_20180420_SEMSDW_B4k.nifti
    elif [ $bval == "07" ]; then
        folder=$f/70_20180329_SEMSDW_B7k.nifti
    else
        folder=$f/70_20180329_SEMSDW_B10k.nifti
    fi

    #$p/process_im.sh $bval $folder
    # - Relabels the images RL AP IS
    # - Separates warm up images and b0's from diffusion weighted
    # - Merges exp together dir*.nii.gz includes all DW with b0's interspersed.

    $p/../gibbs_corr.sh $folder
    # - runs 3D Gibbs correction method proposed by Kellner 2016

    if [ $bval != "04" ]; then
        $p/register_sets.sh $bval $folder
    else
        ln -s $folder/data_gibbsCorr.nii.gz $folder/data_gibbsCorr_reg.nii.gz
    fi
    # - Divides the b0 images into 'sets' based on the central frequency of the scanner. Averages the b0's in each set. Uses flirt to register # all sets in b10k and b7k to the first image in b10k. Uses least square optimisation and spline interpolation.

    # Detrend data
    $p/../detrend_dwi.py -d $folder/data_gibbsCorr_reg.nii.gz -b $folder/bvals -o $folder/data_gibbsCorr_reg_detrend.nii.gz

    # Normalise to b10k data
    if [ $bval != "10" ]; then
        $p/../equate_b0s.py -d ${folder}/data_gibbsCorr_reg_detrend.nii.gz -m ${f}/../masks/nodif_brain_mask.nii.gz -b ${folder}/bvals -r /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/data.nii.gz -rm /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm//data/nodif_brain_mask.nii.gz -rb /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/bvals -o ${folder}/data_gibbsCorr_reg_detrend_norm.nii.gz
    else
        ln -s ${folder}/data_gibbsCorr_reg_detrend.nii.gz ${folder}/data_gibbsCorr_reg_detrend_norm.nii.gz
    fi

    # Link data to main folder
    mkdir $f/../data
    ln -s $folder/data_gibbsCorr_reg_detrend_norm.nii.gz $f/../data/data.nii.gz

    ln -s $folder/bvals $f/../data/bvals
    ln -s $folder/bvecs $f/../data/bvecs
    cd $f/../data
    ln -s ../masks/nodif_brain_mask.nii.gz
    ~/func/extract_shell.py -d data.nii.gz -b bvals -v bvecs -o b0k.nii.gz
    fslmaths b0k -Tmean S0
    rm b0k.*
    if [ $bval == "10" ]; then
        ln -s $folder/registration/ref.nii.gz b0k_ref.nii.gz
    fi

done
