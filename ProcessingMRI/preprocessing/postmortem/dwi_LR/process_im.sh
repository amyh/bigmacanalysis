#!/bin/sh
set -e
set -o errexit

# - Relabels the images RL AP IS
# - Separates warm up images and b0's from diffusion weighted
# - Merges exp together dir*.nii.gz includes all DW with b0's interspersed.

# AH Apr 2018 edited Sep 2021
# NOTE THIS WAS NOT TESTED SEP 2021 SO MAY NEED DEBUGGING

# Specify b-value and directory by argument ie ./process_im.sh 04 70_20180420_SEMSDW_B4k_orig.nifti
# Or can loop over all shells

# Note, in b10k data, we ran exp 23 twice rather than exp 33, so exp 33 was ran at the end. This has consequently been accounted for.

echo "Merging files."

bval=$1
ddir=$2

# Rename b4k to match other file formats
if [[ $bval == "04" ]]; then
    ./rename_b4k.sh $1
fi

cd ${ddir}

# Relabel images RL AP IS
for folder in ./*; do
    file=$folder/MG/image001.nii
    # Rename files .nii.gz (instead of .nii)
    fslchfiletype NIFTI_GZ $file
    file=$file.gz
    cp $file ${file/image/tmp}
    fslorient -deleteorient $file
    if [[ $bval == "04" ]]; then
        # b4k brain was oriented differently in the scanner so requires a
        # different transform
        fslswapdim $file -y z -x $file
    else
        fslswapdim $file y z x $file
    fi
    # Original code
    # fslorient -setqformcode 1 $file
    fslcpgeom ${file/image/tmp} $file -d
    fslorient -setqform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 $file
    fslorient -copyqform2sform $file
    rm ${file/image/tmp}
done


# Specify number of files in each shell
if [[ ${bval} == "04" ]]; then N=23;
else; N=85; fi

mkdir ./procparFiles
for ((i=3;i<10;i=i+2)); do cp NS*_00${i}.nifti/procpar ./procparFiles/procpar_000${i}; done
for ((i=11;i<=N;i=i+2)); do cp NS*_0${i}.nifti/procpar ./procparFiles/procpar_00${i}; done

# Separate b0 images
mkdir ./b0images
mkdir ./temp
for ((i=3;i<10;i=i+2)); do
    fslsplit ./NS*_00${i}.nifti/MG/im* ./temp/;
    cp ./temp/*00.nii.gz ./b0images/b0_000${i}.nii.gz;
    if (($i == "3")); then
        if [ -a ./temp/*26.nii.gz ]; then
            cp ./temp/*26.nii.gz ./b0images/b0_0003b.nii.gz;
        fi
    fi
    rm ./temp/*
done

for ((i=11;i<=N;i=i+2)); do
    fslsplit ./NS*_0${i}.nifti/MG/im* ./temp/;
    cp ./temp/*00.nii.gz ./b0images/b0_00${i}.nii.gz;
    rm ./temp/*
done

rm -r ./temp

# Separately extract warmup scans from b0
# mkdir ./b0images/warmup
# if [ -a ./b0images/b0_0001* ]; then
#     mv ./b0images/b0_0001* ./b0images/warmup/
# fi
# mv ./b0images/b0_0003* ./b0images/warmup/

# Separate extra b0
# mkdir ./b0images/extrab0/
# if [ -a ./b0images/b0_0085* ]; then
#    mv ./b0images/b0_0085* ./b0images/warmup/
# fi

# Merge b0s
fslmerge -t ./b0images/b0_merged ./b0images/b0_*
echo "Here we have all b0s incl from warmup scan, and single b0 at end of file" > ./b0images/README.txt

# Delete extra b0 in b10k (as do not have in b7k)
if [ -a ./NS1_AK5_G_3327_70_semsdw_1k_085.nifti ]; then
    rm -r ./NS1_AK5_G_3327_70_semsdw_1k_085.nifti
fi

# Delete b10k file with wrong gradients
if [ -a ./NS1_AK5_G_3327_70_semsdw_1k_069.nifti.original ]; then
    rm -r NS1_AK5_G_3327_70_semsdw_1k_069.nifti.original
fi

# Separate warmup scans from 1000 dir
mkdir ./warmup

mv ./NS1_AK5_G_3327_70_semsdw_1k_003.nifti ./warmup

fslmerge -t data NS*/MG/im*gz

# Remove original files to save memory
rm -r ./NS*
# can always retrieve from compressed

if [ $bval == "07" ]; then
    # Add slices of zero-filled voxels to either end to make b7k same dimensions as b10k
    fslroi data zeroSlice 0 -1 0 1 0 -1 0 -1
    fslmaths zeroSlice -mul 0 zeroSlice
    fslmerge -y data zeroSlice data zeroSlice
    rm zeroSlice
fi

echo "File merging complete"
