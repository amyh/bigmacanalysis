#!/bin/sh
set -e
set -o errexit

# Correct data for gibbs ringing
# AH Sep 2021

# "This implements the 3D extension to the unringing method proposed by Kellner
# et al. (2016) [1]. This was presented by Thea Bautista at ISMRM 2021 [2].""
# https://github.com/jdtournier/mrdegibbs3D
folder=$1
out=$2
if [ -z "$out" ]; then
  out=data
fi

module add mrdegibbs3d

deGibbs3D -force $folder/${out}.nii.gz $folder/${out}_gibbsCorr.nii.gz
