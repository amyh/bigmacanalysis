# Pipeline to preprocess the bssfp data

folder=/vols/Data/BigMac/MRI/Postmortem/struct/bSSFP/raw/NS1_AK5_G_3327_70_ssfp3d_v4_SOS_046.nifti/

# Convert to .nii.gz
fslchfiletype NIFTI_GZ $folder/MG/sqrtsos001.nii $folder/bssfp.nii.gz

# Sort orientations
file=$folder/bssfp_reorient.nii.gz
cp $folder/bssfp.nii.gz $file
fslorient -deleteorient $file
fslswapdim $file z -x -y $file
fslcpgeom $folder/bssfp.nii.gz $file -d
fslorient -setqform 0.6 0 0 0 0 0.6 0 0 0 0 0.6 0 0 0 0 1 $file
fslorient -copyqform2sform $file

# DeGibbs
../gibbs_corr.sh $folder bssfp_reorient

mkdir $folder/../../data
cd $folder/../../data
ln -s ../raw/NS1_AK5_G_3327_70_ssfp3d_v4_SOS_046.nifti/bssfp_reorient_gibbsCorr.nii.gz struct.nii.gz

# Still see a fair amount of gibbs ringing, both here and in the MGE data
# Can apply gibbs correction multiple times to reduce it, but it also comprimises image quality

# Generate brain mask. Use structural mask as starting bssfp_reorient
# Dilate mask and threshold
fslmaths ../../MGE/masks/brain_mask.nii.gz -dilM tmp
fslmaths tmp -dilM tmp2
fslmaths struct.nii.gz -mul tmp2 tmp3
fslmaths tmp3 -thr 300000 -bin brain_mask
ffslmaths brain_mask.nii.gz -fillh brain_mask.nii.gz
fslmaths brain_mask.nii.gz -add tmp brain_mask.nii.gz
fslmaths brain_mask.nii.gz -add tmp -bin brain_mask.nii.gz
mkdir ../masks
fslmaths struct.nii.gz -mul brain_mask.nii.gz struct_brain
mv brain_mask.nii.gz ../masks/
rm tmp*
