#!/bin/sh
set -e
set -o errexit

# Preprocessing b4k 0.6 mm diffusion data

# Note all original files can be found at /vols/Data/BigMac/MRI/Postmortem/.raw/stdProtocol/
p=/vols/Data/BigMac/MRI/Postmortem/scripts/preprocessing/dwi_HR/
folder=/vols/Data/BigMac/MRI/Postmortem/dwi/b04k/0.6mm/raw/NS1_AK5_G_3327_70_sems/

$p/process_im.sh $folder
# - extracts diffusion volumes (+bvals/bvecs) from folders taken off the scanner
# - reorient data and correct LR AP IS labels

$p/../gibbs_corr.sh $folder
# - correct images for gibbs ringing

# Create rough brain mask from b0 images
fslroi data_gibbsCorr b0k 0 -1 0 -1 0 -1 128 8
fslmaths b0k -Tmean b0k_mean
fslmaths b0k_mean -thr 2000 -bin nodif_brain_mask
fslmaths nodif_brain_mask -fillh nodif_brain_mask

$p/b0_correction.sh $folder
# b0s acquired at end of scan => observe b0 images to shift along z axis
# - linear registration of b0s
# - normalise signal intensity to first b0

# Detrend data
$p/../detrend_dwi.py -d $folder/data_gibbsCorr_b0kCorr.nii.gz -b $folder/bvals -o $folder/data_gibbsCorr_b0kCorr_detrend.nii.gz

# Normalise to b10k data
$p/../equate_b0s.py -d ${folder}/data_gibbsCorr_b0kCorr_detrend.nii.gz -m ${folder}/../../masks/nodif_brain_mask.nii.gz -b ${folder}/bvals -r /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/data.nii.gz -rm /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm//data/nodif_brain_mask.nii.gz -rb /vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/bvals -o ${folder}/data_gibbsCorr_b0kCorr_detrend_norm.nii.gz

# Copy data
cd $folder
mkidr -p ../../data
ln -s $folder/data_gibbsCorr_b0kCorr_detrend_norm.nii.gz ../../data/data.nii.gz
ln -s $folder/bvecs ../../data/
ln -s $folder/bvals ../../data/

# Create mean b0k image
cd ../../data
ln -s ../masks/nodif_brain_mask.nii.gz
~/func/extract_shell.py -d data.nii.gz -b bvals -v bvecs -o b0k.nii.gz
fslmaths b0k -Tmean S0
rm b0k.*
