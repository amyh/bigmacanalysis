#!/bin/sh
set -e
set -o errexit

# Extracts dMRI data.
# Aligns data to fsl coordinates.

# AH edited Sep 2021

folder=$1
cd $folder

# Merge together diffusion scans (23,25,27,29 + 31-38 b0's)
cp NS1_AK5_G_3327_70_sems_023.nifti/MG/image001.nii data.nii
for i in 25 27 29 31 32 33 34 35 36 37 38; do
  cp NS*_0${i}.nifti/MG/image001.nii ./;
  fslmerge -t data.nii  data.nii image001;
done
rm image001.nii

fslchfiletype NIFTI_GZ NS1_AK5_G_3327_70_sems_023.nifti/MG/image001.nii data.nii.gz
for i in 25 27 29 31 32 33 34 35 36 37 38; do
  fslchfiletype NIFTI_GZ NS*_0${i}.nifti/MG/image001.nii image001.nii.gz;
  fslmerge -t data data image001;
done
rm image001.nii.gz

# Compress
fslchfiletype NIFTI_GZ data.nii

# Extract procpar files - from which can obtain b-vals and bvecs
mkdir procparFiles
for i in 23 25 27 29 31 32 33 34 35 36 37 38;
  do cp NS*_0${i}.nifti/procpar procparFiles/procpar_0${i};
  rm -r NS*_0${i}.nifti/
done

# Run ./extract_bvecs.m to extract bvecs from propcar and align with (reoriented) data

# Reorient data (bvecs reoriented in matlab script)
file=./data.nii.gz
fslreorient2std $file $file
fslroi $file temp.nii.gz 0 -1 0 -1 0 -1 130 1
fslorient -deleteorient $file
fslswapdim $file -x z y $file       #is this correct? if not try  z -x -y
fslcpgeom temp.nii.gz $file -d
fslorient -setqform 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 $file
fslorient -copyqform2sform $file
rm temp.nii.gz


# Check bvecs
mkdir dtifit
dtifit -k ./data -o ./dtifit/dti -m ./nodif_brain_mask -b ./bvals -r ./bvecs
