%% Take bvals and bvecs from scanner and print to txt file
% Matlab script
% AH Apr 2019

close all, clear all
files = dir('./procparFiles/p*');

%%
bvals = [];
% Including all files (+warmup and final b0)
for i = 1:numel(files)
    filename = ['./procparFiles/' files(i).name];
    file = fopen(filename,'r');
    formatspec = '%s';
    A = textscan(file,formatspec,'Delimiter',' ');
    fclose(file);
    a = string([A{:}]);
    % Find info about bvalues
    ind = find(a == 'bvalue');
    % They begin 12 values after
    ind = ind+12;
    % Collect bvalues
    if i<5
        b = a(ind:ind+31);
    else
        b = a(ind);
    end

    bvals = [bvals;str2double(b)];
end

% Write to text file
% N = 1000;
% file = fopen(['./' num2str(N) 'bvals.txt'],'w');
file = fopen('./bvals','w');
formatspec = '%f \n';
for i = 1:size(bvals)
    fprintf(file,formatspec,bvals(i));
end
fclose(file);

%% Repeat for bvecs
bvecs = [];
% bvecs stored in procpar files under dro dpe dsl

dro = []; dpe = []; dsl = [];

for i = 1:numel(files)
    filename = ['./procparFiles/' files(i).name];
    file = fopen(filename,'r');
    formatspec = '%s';
    A = textscan(file,formatspec,'Delimiter',' ');
    fclose(file);
    a = string([A{:}]);
    % Find info about dro
    for v = ["dro","dpe","dsl"]
        ind = find(a == v);
        % They begin 12 values after
        ind = ind+12;
        % Collect values
        if i < 5, subset = a(ind:ind+31);     % Warmup scan twice as long
        else, subset = a(ind); end
        if v == "dro", dro = [dro;subset]; end
        if v == "dpe", dpe = [dpe;subset]; end
        if v == "dsl", dsl = [dsl;subset]; end
    end
    % dro = x, dpe = y, dsl = z
end
bvecs = [dro,dpe,dsl];

% Realign
% bvecs = rot_z(pi)*rot_y(pi/2)*rot_x(pi)*rot_z(pi)*rot_x(pi/2)*str2double(bvecs)';
bvecs = rot_z(pi)*rot_y(3*pi/2)*rot_x(pi/2)*str2double(bvecs)';

bvecs = bvecs';

% Write to text file
% N = 1000;
% file = fopen(['./' num2str(N) 'bvals.txt'],'w');
file = fopen('./bvecs','w');
formatspec = '%f %f %f \n';
for i = 1:size(bvals)
    fprintf(file,formatspec,[bvecs(i,1),bvecs(i,2),bvecs(i,3)]);
end
fclose(file);



%%%%%%%%%%%%%
function mat = rot_z(x)
% Creates rotation matrix for angle x (clockwise) about the z axis
mat = [cos(x) -sin(x) 0;
      sin(x)  cos(x) 0;
        0       0    1;];

end % EOF

%%%%%%%%%%%%%
function mat = rot_x(x)
% Creates rotation matrix for angle x (clockwise) about the x axis
mat = [ 1    0      0   ;
        0 cos(x) -sin(x);
        0 sin(x)  cos(x);];

end % EOF

%%%%%%%%%%%%%
function mat = rot_y(x)
% Creates rotation matrix for angle x (clockwise) about the z axis
mat = [cos(x) 0 sin(x);
        0     1      0;
      -sin(x) 0 cos(x);];

end % EOF
