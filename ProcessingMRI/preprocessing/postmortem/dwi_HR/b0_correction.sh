

# Correct for signal drift - motion correction with flirt. Intra-modaility cost
# function (normcorr). Limit to only z-axis displacement (flirt schedule file)
# Normalise to first b0 - most likely representative of the "true" b0 for the
# dwi volumes. (b0 may also change due to temp)

# AH 29/11/2019 - edited Sep 2021


folder=$1
cd $folder

mkdir tmp
fslsplit b0k.nii.gz ./tmp/tmp -t
cd tmp

# Register to mean image from dw volumes
fslroi ../data_gibbsCorr.nii.gz dwi.nii.gz 0 -1 0 -1 0 -1 0 128
fslmaths dwi -Tmean dwi_mean

for i in {0..7}; do
    flirt -in tmp000${i}.nii.gz -ref dwi_mean.nii.gz -schedule /vols/Data/BigMac/MRI/Postmortem/scripts/FLIRTconfig/ztransonly.sch -out tmp000${i}_reg -omat tmp000${i}_reg.mat -interp spline -cost corratio;
done

fslmaths tmp0000_reg.nii.gz -mul ../nodif_brain_mask.nii.gz tmp0000_reg_brain.nii.gz

mean=`fslstats  tmp0000_reg_brain.nii.gz -M`

for i in {1..7}; do
    fslmaths tmp000${i}_reg.nii.gz -mul ../nodif_brain_mask.nii.gz tmp000${i}_reg_brain.nii.gz
    m=`fslstats  tmp000${i}_reg_brain.nii.gz -M`
    fslmaths tmp000${i}_reg.nii.gz -div $m -mul $mean tmp000${i}_corr.nii.gz
done


fslmerge -t ../b0k_corr.nii.gz tmp0000_reg.nii.gz *_corr.nii.gz

fslmerge -t ../data_gibbsCorr_b0kCorr.nii.gz ./dwi.nii.gz ../b0k_corr.nii.gz

cd ../
rm -r tmp
rm b0k_corr.nii.gz b0k_mean.nii.gz b0k.nii.gz
