#!/usr/bin/env fslpython

# Remove linear trend from data on a voxelwise basis
# Fit to b0s and dwi volumes separately
# datfile   input data
# bfile      bvals
# ofile      output filename

import numpy as np
import nibabel as nib
import os.path as op
import argparse


def normalise(x,axis=0):
    return (x-np.mean(x,axis=axis))/np.std(x,axis)/np.sqrt(x.size)

def detrend(y,order=1):
    '''
       detrend data along second axis

    Parameters:
    y : array-like
    order : integer

    Returns:
    array-like
    '''
    x = np.linspace(0,1,y.shape[1])
    desmat = []
    for i in range(order+1):
        r = x**i
        if i>0:
            r = normalise(r)
        desmat.append(r.flatten())
    desmat = np.asarray(desmat).T
    beta = np.linalg.pinv(desmat)@y.T
    yc   = y - (desmat@beta).T + np.mean(y,axis=1)[:,None]

    return yc


def main():
    p = argparse.ArgumentParser(description='Detrend DWI data')

    p.add_argument('-d','--data',
                  required=True,type=str,metavar='VOLUME',
                  help='input data')
    p.add_argument('-b','--bval',
                   required=True,type=str,metavar='<str>',
                   help='bvals text file')
    p.add_argument('-o','--output',
                   required=True,type=str,metavar='VOLUME',
                   help='output data')
    p.add_argument('--approx',required=False,default=500,
                   help='range of equal bvals (default=500)') # Changed from 5
    p.add_argument('--order',required=False,default=1,
                   help='polynomial order')

    args = p.parse_args()

    # Load data
    filename = op.expandvars(args.data)
    imobj    = nib.load(filename,mmap=False)
    im       = imobj.get_fdata().astype(float)
    b        = np.loadtxt(args.bval)

    # round bvalues
    if args.approx > 0:
        b = np.round(b/(2.0*args.approx))*args.approx


    # Reshape data
    y = im.reshape((-1,im.shape[3]))

    # Loop over separate shells
    ub = np.unique(b)

    print('Processing shell (bval):')
    for bval in ub:
        print(2*bval)
        dwi = y[:,b==bval].copy()
        dwi = detrend(dwi)
        y[:,b==bval] = dwi.copy()


    im = y.reshape(im.shape)

    # Save new image
    newhdr = imobj.header.copy()
    newobj = nib.nifti1.Nifti1Image(im, None, header=newhdr)
    nib.save(newobj,args.output)



if __name__ == '__main__':
    main()
