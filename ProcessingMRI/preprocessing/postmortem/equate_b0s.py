#!/usr/bin/env fslpython
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 18:36:26 2019

@author: amyh
"""
import numpy as np
import nibabel as nib
import os.path as op
import argparse

def mean_b0(im,mask,b,args):
    mask[mask==0] = np.nan
    
    # round bvalues
    if args.approx > 0:
        b = np.round(b/(2.0*args.approx))*args.approx
        
    # Extract b0s
    m = np.unique(b).min()
    b0s = im[:,:,:,b==m].copy()*np.expand_dims(mask,3)
        
    m = np.nanmean(b0s[:])
    return m

 
def load(filename):

    # Load data
    filename = op.expandvars(filename)
    imobj    = nib.load(filename,mmap=False)
    im       = imobj.get_fdata().astype(float)
    
    return im,imobj
    


def main():
    p = argparse.ArgumentParser(description='Equate mean b0 between given and reference dataset')

    p.add_argument('-d','--data',
                  required=True,type=str,metavar='VOLUME',
                  help='input data')
    p.add_argument('-m','--mask',
                  required=True,type=str,metavar='VOLUME',
                  help='input mask')
    p.add_argument('-b','--bval',
                   required=True,type=str,metavar='<str>',
                   help='bvals text file')
    p.add_argument('-r','--ref',
                  required=True,type=str,metavar='VOLUME',
                  help='reference data')
    p.add_argument('-rm','--refmask',
                  required=True,type=str,metavar='VOLUME',
                  help='reference mask')
    p.add_argument('-rb','--refbval',
                   required=True,type=str,metavar='<str>',
                   help='reference bvals text file')
    p.add_argument('-o','--output',
                   required=True,type=str,metavar='VOLUME',
                   help='output data')
    p.add_argument('--approx',required=False,default=500,
                   help='range of equal bvals (default=500)') # Changed from 5


    args = p.parse_args()
    
    im,imobj = load(args.data)
    mask = load(args.mask)[0]
    b = np.loadtxt(args.bval) 
    ref = load(args.ref)[0]
    refmask = load(args.refmask)[0]
    refb = np.loadtxt(args.refbval)  
    
    meanim = mean_b0(im,mask,b,args)
    meanref = mean_b0(ref,refmask,refb,args) 
    
    imcorr = im / meanim * meanref

    # Save new image
    newhdr = imobj.header.copy()
    newobj = nib.nifti1.Nifti1Image(imcorr, None, header=newhdr)
    nib.save(newobj,args.output)
    
if __name__ == '__main__':
    main()