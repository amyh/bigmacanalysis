#!/bin/bash

# AH - updated March 2022

# Register T1-like structural to F99 image as structural MRI has inverted (T2-like) contrast
# Here we use the T1-like image (T1-like2) where the wm/gm are combined using
# hand drawn masks. This is because when we simply invert all contrast, we end
# up with very bright regions in the ventricles / between gyri, which can
# cause errors in the registratin (as the csf is dark in the F99 data)

# We use brain extracted data for flirt, and non-brain extracted data for fnirt,
# where we instead input both an input and reference mask, so that regions
# outside the brain do not contribute to the cost. Note, both masks include
# the CSF which is now dark in both images. The McLaren brain mask is much
# better than the standard F99 mask which appears to be missing frontal regions

in=/vols/Data/BigMac/MRI/Postmortem/struct/data/T1-like/T1-like_2_with-background.nii.gz
in_brain=/vols/Data/BigMac/MRI/Postmortem/struct/data/T1-like/T1-like_2.nii.gz
inmask=/vols/Data/BigMac/MRI/Postmortem/struct/masks/brain_mask_filled.nii.gz
reg=/vols/Data/BigMac/MRI/Postmortem/struct/reg
ref=/vols/Data/BigMac/MRI/Standard/F99/data/mri/F99.nii.gz
ref_brain=/vols/Data/BigMac/MRI/Standard/F99/data/mri/F99_brain.nii.gz
refmask=/vols/Data/BigMac/MRI/Standard/F99/McLaren/McLaren_brain_mask.nii.gz
config=$FSLDIR/data/xtract_data/standard/F99/config
base=struct_2_F99

# FLIRT
echo Running FLIRT
o1=$reg/${base}_flirt/
#mv $o1 $reg/OldFiles
mkdir $o1
#flirt -in $in_brain -ref $ref_brain -omat $o1/${base}_flirt.mat -out $o1/${base}_flirt -inweight $inmask
flirt -in $in_brain -ref $ref_brain -omat $o1/${base}_flirt.mat -out $o1/${base}_flirt

# FNIRT
echo Running FNIRT
o2=$reg/${base}_fnirt/
# $o2 $reg/OldFiles
mkdir $o2
fnirt --ref=$ref --in=$in --aff=$o1/${base}_flirt.mat --inmask=$inmask --refmask=$refmask --cout=$o2/${base}_fnirt_coeffs --iout=$o2/${base}_fnirt --fout=$o2/${base}_fnirt_field --jout=$o2/${base}_fnirt_jac --config=$config

# Invert
echo Inverting warp
baseinv=F99_2_struct
o3=/vols/Data/BigMac/MRI/Standard/F99/reg/${baseinv}_fnirt
mkdir $o3
#mv $o3 $reg/OldFiles
mkdir $o3
invwarp --ref=$in_brain --warp=$o2/${base}_fnirt_field --out=$o3/${baseinv}_fnirt_field
applywarp --ref=$in --in=$ref --warp=$o3/${baseinv}_fnirt_field --out=$o3/${baseinv}_fnirt
