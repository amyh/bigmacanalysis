#!/bin/bash
in=$1
out=$2

dtifit --data=$in/data.nii.gz --out=$out/dti --mask=$in/nodif_brain_mask.nii.gz --bvecs=$in/bvecs --bvals=$in/bvals
