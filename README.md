# BigMacAnalysis

Analysis scripts for the BigMac dataset.

Further details about the dataset can be found in
the online documentation: https://open.win.ox.ac.uk/pages/amyh/bigmacdocumentation/
or via the manuscripts cited below.

Here we provide tutorials and analysis scripts to help others
get started when working with the BigMac data and to reproduce 
Figures from the paper "An open resource combining 
multi-contrast MRI and microscopy in the macaque brain", 
Nat. Comms. 2023. Note, scripts for creating 3D hybrid fibre orientations (Figure 7) 
are currently not made openly available on gitlab as this is the subject 
of active, ongoing work.

See below for details of what is included in each folder.
Contact details are provided at the bottom of the page.

## Getting started
Please download the repository using git clone. Accompanying data (zip files) can be 
downloaded from http://dx.doi.org/10.5287/ora-jvxdvrogr. Note, this data
is meant solely for the use of recreating the analyses presented and cannot
be used for original research or any other means.

Where *data.zip files are provided, please unzip and copy the contents 
(BigMac folder and any other files) into the local folder. 
As plots require the cbrewer colour scheme, please ensure this folder is 
added to the MATLAB path.

Run times should be < several minutes unless indicated otherwise. 
Prebaked outputs are provided for any functions which take more than a 
few minutes to run. No non-standard hardware is required, though GPUs
are beneficial when running certain analyses such as eddy preprocessing,
or the ball and stick model / tractography.

This code has been ran on a Mac using MATLAB 2019a-2021a and python 3. 
MRI-microscopy co-registration was performed using TIRL V2.1.3b1 which is 
openly available as part of FSL and can be downdloaded via git at 
https://git.fmrib.ox.ac.uk/ihuszar/tirl. MRI data were analysed using the 
FMRIB Software Library (FSL 6.0.5/6.0.6) which is openly available via 
https://fsl.fmrib.ox.ac.uk/fsl/fslwiki. See below for details about 
additional software for data preprocessing. 

## Citation
##### Paper
Howard AFD et al., An open resource combining 
multi-contrast MRI and microscopy in the macaque brain, 
Nature Communications, 2023

Preprint:
Howard AFD et al., The BigMac dataset: an open resource combining 
multi-contrast MRI and microscopy in the macaque brain, bioRxiv, 2022, 
doi: 10.1101/2022.09.08.506363

##### This git repo
Howard AFD et al. The BigMac dataset: analysis scripts, Zenodo 2023, 
doi: 10.5281/zenodo.7920662 


## Folder contents

### Comparing MRI and Microscopy

**Coregistered ODFs (Figure 6)**  
Comparing 2D fibre orientation distribution functions from MRI and 
microcopy. (TIRL/MATLAB)

This code requires microscopy data which has already been coregistered to 
the dwi using the functions provided in the TIRL folder. Example data is 
provided in CoregisteredODFs-data.zip via the doi above.

### Processing MRI
Scripts for minimal preprocessing of the MRI data. (FSL/MRtrix/qMRLab/md-dmri/MrCat)

MRI data were analysed using the FMRIB Software Library (FSL 6.0.5/6.0.6) 
and the scripts provided here. Mrtrix3 was used for Gibbs ringing 
correction: https://github.com/jdtournier/mrdegibbs3D. In vivo MRI were 
preprocessed using the MR comparative anatomy toolbox (MrCat) scripts at 
https://github.com/neuroecology/MrCat. T1maps were generated using qMRLab 
https://qmrlab.readthedocs.io/en/master/ (V2.4.1) and MATLAB 2021a. 
Diffusion data with multiple tensor encoding were analysed using the 
multi-dimensional diffusion MRI toolbox, 
https://github.com/markus-nilsson/md-dmri and MATLAB 2019a.

### TIRL 
Mapping between MRI and microscopy domains. (python3/TIRL)

Here we provide both a jupyter notebook tutorial to demonstrate how to 
map between domains using TIRL, as well as scripts to perfrom this mapping 
in the BigMac dataset. Further, we provide a jupyter notebook that allows the user
to assess the quality of the TIRL registration for any specific microscopy slide of 
interest. Instructions and example config files are also provided so that users
can run the registration themselves, e.g. over a small region of interest or if
wanting to register sections from the cerebellum where warpfields are not currently
provided.

Requires TIRL to be installed. Please see above for details.

### Ultra HARDI 
Analyis of ultra high angular resolution diffusion MRI. (MATLAB)
Data is provided in UltraHARDI-data.zip via the doi above.

**BAS (Figure 2b)**  
Comparing outputs from the Ball and Stick model for different b-values and 
number of gradient directions. (FSL/MATLAB)

This analysis requires the diffusion MRI data to be preprocessed using the 
ball and stick model. Ball and stick outputs are provided for different 
subsets of data (i.e. data with different angular resolution) in **data.zip.

**Connectivity Matrices (Figure 2c)**  
Comparing the output from tractography for data with different angular 
resolution. (FSL/MATLAB)

This analysis requires data to be processed using ball and stick and 
probtrackx to create estimates of the structural connectome. This output 
is provided in **data.zip. 

**fsl_scripts**  
Scripts for running bedpostx (Ball and Stick model) or probtrackx 
(tractography) for RM network analysis of the structural connectome.
Ideally these scripts are ran on a GPU to speed up processing times.

### cbrewer
Figures from the paper use the cbrewer colour scheme included here. Please
ensure this folder is added to the matlab path.


## Contact us?
Questions or feedback are very welcome. Please contact Amy Howard:
amy.howard@ndcn.ox.ac.uk  https://www.win.ox.ac.uk/people/amy-howard

