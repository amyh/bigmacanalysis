"""

Using TIRL to map BigMac microscopy data (coords, inplane angle and microscopy normal)
into b10k dwi space.

More information on TIRL can be found at https://www.biorxiv.org/content/10.1101/849570v1.full.pdf
In BigMac, TIRL has been run on every microscopy slide in turn, to create a warpfield between the microscopy and structural MRI.
Here the structural MRI is used as reference due to its high spatial resolution and good image contrast (clear gm/wm delineation).
MRI-MRI warpfields generated using FSLs FLIRT/FNIRT can be used to map between the microscopy and other MRI contrasts such as dMRI.

Created on Nov 24 2021
@author: amyh

"""

import tirl
import numpy as np
from tirl import fsl
from tirl.timage import TImage
import os
import sys
from PIL import Image
import nibabel as nib
from TIRLmapping_BigMac import *
import shutil

slideno = sys.argv[1]   #H092a or CV092a or P090x etc
regfile = sys.argv[2]   #Reg2MRI
rowstart = int(sys.argv[3])
rowstop = int(sys.argv[4])
stain = sys.argv[5]		# Gallyas / Cresyl
half = sys.argv[6]      # Anterior/Posterior

# FNIRT transformations, struct -> dwi (opposite to what you think you need)
dwi = sys.argv[7]
field = sys.argv[8]
dwiout = sys.argv[9]    # suffix for output
# e.g. if mapping to dwi b10k space
# dwi = "/vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/S0.nii.gz"
# field = "/vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/reg/dwi_1.0mm_2_struct_fnirt/dwi_1.0mm_2_struct_fnirt_field.nii.gz"
# dwiout = "dwi_1.0mm_b10kspace"


if stain=="PLI":
    # Base folder
    base = "/vols/Data/BigMac/Microscopy/"+stain+"/"+half+"/"+slideno[:-1]+"x/"
    # High resolution PLI images
    inplane_tif = base+"/In_plane/mosaic.tif"
else:
    # Base folder
    base = "/vols/Data/BigMac/Microscopy/"+stain+"/"+half+"/"+slideno[:-1]+"x/structureTensor/"+slideno+"_ST_150/"
    # High resolution histology images
    inplane_tif = base+"/"+slideno+"_O.tif"


# If microscopy has been registered in two halfs (i.e. the hemispheres
# are separated), then loop over both hemispheres

regorig = regfile
loop = 1;
while loop>0:

    if stain=='PLI':
        f = base+"/Masks/"
    else:
        f = base

    if os.path.isdir(base+"/"+regorig+"/Left") and loop==1:
        regfile = regorig+"/Left/"
        mask_tif = f+slideno+"_L_mask.tif"
        loop = 2
        print("Processing left mask")
    elif os.path.isdir(base+"/"+regorig+"/Right") and loop==2:
        regfile = regorig+"/Right/"
        mask_tif = f+slideno+"_R_mask.tif"
        loop = 0
        print("Processing right mask")
    else:
        mask_tif = f+slideno+"_mask.tif"
        loop = 0
        print("Processing full slide")

    if not os.path.isdir(base+"/"+regfile+"/VectorMapping"):
        os.mkdir(base+"/"+regfile+"/VectorMapping")

    if rowstart==0:
        ofile = base+"/"+regfile+"/VectorMapping/"+dwiout+"_coords_x.tif"
    else:
        ofile = base+"/"+regfile+"/VectorMapping/"+dwiout+"_norm_x.tif"

    if not os.path.isfile(ofile):
        micro_2_mri(base,regfile,inplane_tif,mask_tif,rowstart,rowstop,dwiout,field,dwi)

# Subsequently run 'merge' file to stick rows back together
