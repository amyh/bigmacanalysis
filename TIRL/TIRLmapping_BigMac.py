
import tirl
import numpy as np
from tirl import fsl
from tirl.timage import TImage
import os
import sys
from PIL import Image
import nibabel as nib
import shutil

def micro_inplane_2_mri(inplane,chain,mask,fout,rowstart,rowstop):

    # Map in-plane angles through transformation chain

    # Change scale 0-pi
    inplane.normalise()
    inplane = inplane*np.pi
    s1,s2 = inplane.shape

    # Convert pli angle to x,y coordinates
    # note fsleyes shows fsl orientations WITH LR flip!!!
    # Redefined 25/08/2022 - TIRL does not account for last LR flip for FSl vectors. => do this manually after mapping, otherwise rotations from the warp field are in the wrong sense clockwise/anticlockwise.
    x = -np.sin(inplane.data[:]).ravel()
    y = np.cos(inplane.data[:]).ravel()
    xy = np.c_[x, y]
    del x, y

    # side note, iterate over chunks of pixels to avoid memory issues
    chunksize = int(1e5)
    n_pixels = s1*s2

    print("Mapping inplane vectors...")
    file = fout+'_row'+str(rowstart)+'_'+str(rowstop)+'.txt'
    dwivox_coords = inplane.domain.get_voxel_coordinates()

    # Delete files if they already exist
    if os.path.isfile(file):
        os.remove(file)
    with open(file, "ab") as f:
        print(file,flush=True)
        for start in range(rowstart*s2,rowstop*s2,chunksize):
            if start>n_pixels: break
            stop = min(n_pixels, rowstop*s2, start + chunksize)
            print(start,stop,flush=True)
            # map pli angles
            v = xy[start:stop,:]
            c = dwivox_coords[start:stop,:]
            # only map vectors inside tissue mask
            m = mask[start:stop]
            # fill masked areas in output with zeros
            o = np.zeros((c.shape[0],3))
            if m[m==1].size>0:
                dwivox_vect = chain.map_vector(v[m==1,:], c[m==1,:], rule="fs")
                o[m==1,:] = dwivox_vect*[-1,1,1]    # Add final LR flip so storage of vectors hassame convention as FSL
                del dwivox_vect
            # save output to txt file
            np.savetxt(f,o,fmt="%.3f")
            del v, c, m, o

    print("Done mapping inplane",flush=True)


def micro_normal_2_mri(micro_hr,chain,mask,fout,rowstart,rowstop):

    # Map microscopy normal through transformation chain

    # When mapping the microscopy normal, need to separate 2D and 3D transforms
    chain2D = chain[:7]
    chain3D = chain[7:]

    # side note, iterate over chunks of pixels to avoid memory issues
    chunksize = int(1e5)
    s1,s2 = micro_hr.shape
    n_pixels = s1*s2

    print("Mapping microscopy normal...")
    file = fout+'_row'+str(rowstart)+'_'+str(rowstop)+'.txt'
    dwivox_coords = micro_hr.domain.get_voxel_coordinates()

    # Delete files if they already exist
    if os.path.isfile(file):
        os.remove(file)
    with open(file,"ab") as fnorm:
        print(file,flush=True)
        for start in range(rowstart*s2,rowstop*s2,chunksize):
            if start>n_pixels: break
            stop = min(n_pixels, rowstop*s2, start + chunksize)
            print(start,stop,flush=True)
            # map norm
            c = dwivox_coords[start:stop,:]
            norm = np.zeros((c.shape[0],3))
            norm[:,2] = 1
            # only map vectors inside tissue mask
            m = mask[start:stop]
            # fill masked areas in output with zeros
            o = np.zeros(norm.shape)
            if m[m==1].size>0:
                dwivox_coords2D = chain2D.map(c[m==1,:])
                dwivox_norm = chain3D.map_vector(norm[m==1,:], dwivox_coords2D, rule="fs")
                o[m==1,:] = dwivox_norm*[-1,1,1]    # Add final	LR flip	so storage of vectors hassame convention as FSL
                del dwivox_norm, dwivox_coords2D
            # save output to txt file
            np.savetxt(fnorm,o,fmt="%.3f")
            del norm, c, m, o

    print("Done mapping microscopy normal",flush=True)


def micro_coords_2_mri(micro_hr,mri,chain,dwi,fout):
    # Map coordinates of high resolution image through tirl chain to dwi space
    s1,s2 = micro_hr.shape

    # Extract voxel coordinates for hr microscopy data
    array = micro_hr.domain.get_voxel_coordinates()

    # If output already exists, delete it
    file = fout+'.txt'
    if os.path.isfile(file):
       os.remove(file)

    # Use transformation chain to map pixel coordinates through to dwi space
    # Have to divide up coords as get memory issues converting to mri
    print('Mapping microscopy coordinates...',flush=True)
    N = s1*s2/1e5
    with open(file, "ab") as f:
        for i in np.arange(N):
            if i==N-1:
                a = array[int(i*1e5)::][:]
            else:
                a = array[int(i*1e5):int((i+1)*1e5)][:]
            # Perform mapping
            dwivox_coords = chain.map(a)
            # Print output to txt file
            np.savetxt(f, dwivox_coords,fmt="%.3f")

    # This works well!
    # resave this as a series of tif files
    dwivox_coords = np.reshape(np.loadtxt(file),(s1,s2,3))
    # Normlise coords by dwi dimensions if in dwi space. otherwise by struct
    if dwi=="False":
        dwi = mri
    else:
        dwi = TImage(dwi)
    s1,s2,s3 = dwi.shape
    vectx = Image.fromarray((dwivox_coords[:,:,0]/s1*255).astype(np.float64))
    vectx.save(fout+'_x.tif')
    vecty = Image.fromarray((dwivox_coords[:,:,1]/s2*255).astype(np.float64))
    vecty.save(fout+'_y.tif')
    vectz = Image.fromarray((dwivox_coords[:,:,2]/s3*255).astype(np.float64))
    vectz.save(fout+'_z.tif')
    os.remove(file)
    print('Coordinate mapping complete',flush=True)

def chain_micro_2_mri(micro_timg,mri_timg,field="False",dwi="False"):
    # 1. Load microscopy slide that we have registered to structural MRI space using TIRL
    if os.path.isfile(micro_timg+".timg"):
        if not os.path.isfile(micro_timg+"-copy.timg"):
            shutil.copyfile(micro_timg+".timg",micro_timg+"-copy.timg")
            os.chmod(micro_timg+"-copy.timg",0o777)
        micro = tirl.load(micro_timg+"-copy.timg")
        chain = micro.domain.chain              # (microscopy -> physical)
    else:   # if we only have domain not full timg
       	if not os.path.isfile(micro_timg+"-copy.dom"):
            shutil.copyfile(micro_timg+".dom",micro_timg+"-copy.dom")
            os.chmod(micro_timg+"-copy.dom",0o777)
        micro = tirl.load(micro_timg+"-copy.dom")
        chain = micro.chain			# (microscopy -> physical)

    # 2. Create warpfield from microscopy to structural
    if not os.path.isfile(mri_timg+"-copy.timg"):
        shutil.copyfile(mri_timg+".timg",mri_timg+"-copy.timg")
        os.chmod(mri_timg+"-copy.timg",0o777)
    mri = tirl.load(mri_timg+"-copy.timg")

    # Invert warpfield in mri_timg and add to warpfield micro_timg
    tirl2struct = mri.domain.chain.inverse()
    # (microscopy -> physical -> mri)
    chain += tirl2struct
    del tirl2struct

    # 3. Import FNIRT transformation & add to transformation chain
    # Note, opposite transform to what you might expect due to how fsl defines warpfields
    if field and field != "False":
        warp = fsl.load_warp(field, moving=dwi)
        # (microscopy -> physical -> structural -> dwi)
        chain += warp
        del warp

    print('Chain constructed')
    return chain,micro,mri


def micro_2_mri(base,regfile,inplane_tif,mask_tif,rowstart,rowstop,mriout,field="False",dwi="False"):

    # Map microscopy coords, inplane angle and normal to mri space
    # Maps to structural space unless field and dwi are specified

    # TIRL TImages
    # timg containing information to transform microscopy to physical space (i.e. tirl intemediary space) where the data are coregistered
    micro_timg = base + "/" + regfile + "/user_defined_optimum"
    mri_timg = base + "/" + regfile + "/volume"

    # Load mask
    mask = TImage(mask_tif)
    mask = mask.data.flatten()
    mask[mask>0] = 1

    # Output
    fout = base+"/"+regfile+"/VectorMapping/"
    if not os.path.isdir(fout):
       os.mkdir(fout)
    coords_dwi = fout + "/" + mriout + "_coords"
    vect_dwi = fout + "/" + mriout + "_inplane"
    norm_dwi = fout + "/" + mriout + "_norm"

    # 1. Load data and construct transformation chain microscopy -> dwi
    chain, _, mri = chain_micro_2_mri(micro_timg,mri_timg,field,dwi)
    inplane = TImage(inplane_tif)

    # 2. Map coordinates of high resolution image through tirl chain to dwi space
    if rowstart==0:
        micro_coords_2_mri(inplane,mri,chain,dwi,coords_dwi)

    # 3. Map 3D PLI vector through tirl chain into diffusion space
    # Only map vectors in mask where angles are meaningful
    micro_inplane_2_mri(inplane,chain,mask,vect_dwi,rowstart,rowstop)

    # 4. Also map microscopy normal
    micro_normal_2_mri(inplane,chain,mask,norm_dwi,rowstart,rowstop)
