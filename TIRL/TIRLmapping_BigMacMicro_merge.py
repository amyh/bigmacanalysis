##!/usr/bin/env python

# Author: Amy FD Howard
# Date: 24 Nov 2020, revised 22/11/2021

"""

Read in microscopy vector in dwi space - which have been processed in batches - and
combine into larger image

"""

import tirl
import numpy as np
from tirl import fsl
from tirl.timage import TImage
import sys
from PIL import Image
import os
import glob

def save_output(file,vectors,s1,s2):
    vectors = np.reshape((vectors+1)/2*255,(s1,s2,3))
    vectx = Image.fromarray(vectors[:,:,0].astype(np.float64))
    vectx.save(file+'_x.tif')
    vecty = Image.fromarray(vectors[:,:,1].astype(np.float64))
    vecty.save(file+'_y.tif')
    vectz = Image.fromarray(vectors[:,:,2].astype(np.float64))
    vectz.save(file+'_z.tif')


slideno = sys.argv[1]   # e.g. P090x
regfile = sys.argv[2]
batchsize = int(sys.argv[3])
stain = sys.argv[4]		# e.g. Gallyas / Cresyl
half = sys.argv[5]
dwiout = sys.argv[6]    # e.g. dwi_1.0mm_b10kspace

if stain=="PLI":
    # Base folder
    base = "/vols/Data/BigMac/Microscopy/PLI/"+half+"/"+slideno+"/"
    # High resolution PLI images
    inplane = TImage(base+"/In_plane/mosaic.tif")
else:
    # Base folder
    base = "/vols/Data/BigMac/Microscopy/"+stain+"/"+half+"/"+slideno[:-1]+"x/structureTensor/"+slideno+"_ST_150/"
    # High resolution histology images
    inplane = TImage(base+"/"+slideno+"_O.tif")

s1,s2 = inplane.shape
del inplane

files = [regfile,regfile+"/Left/",regfile+"/Right/"]
for regfile in files:
    if os.path.isfile(base+"/"+regfile+"/VectorMapping/"+dwiout+"_coords_x.tif"):
        if not os.path.isfile(base+"/"+regfile+"/VectorMapping/"+dwiout+"_norm_x.tif"):
            vect_dwi = base+"/"+regfile+"/VectorMapping/"+dwiout+"_inplane"
            norm_dwi = base+"/"+regfile+"/VectorMapping/"+dwiout+"_norm"

            dwivox_vect = np.empty((s1*s2,3),dtype=np.float64)
            dwivox_norm = np.empty((s1*s2,3),dtype=np.float64)
            for start in range(0,s1,batchsize):
                stop = min(s1*s2,start*s2+batchsize*s2)
                dwivox_vect[start*s2:stop,:] = np.loadtxt(vect_dwi+'_row'+str(start)+'_'+str(start+batchsize)+'.txt')[0:stop-start*s2,:]
                dwivox_norm[start*s2:stop,:] = np.loadtxt(norm_dwi+'_row'+str(start)+'_'+str(start+batchsize)+'.txt')[0:stop-start*s2,:]
                print('loop '+str(start)+' completed',flush=True)

            # Save coords as tif files
            # change range -1->1 to 0->1
            save_output(vect_dwi,dwivox_vect,s1,s2)
            save_output(norm_dwi,dwivox_norm,s1,s2)

            # Delete tmp files
            fileList = glob.glob(base+"/"+regfile+"/VectorMapping/*row*txt")
            for filePath in fileList:
                try:
                    os.remove(filePath)
                except:
                    print('Error while deleting file: ', filePath)
            
            fileList = glob.glob(base+"/"+regfile+"/*copy.timg")
            for filePath in fileList:
                try:
                    os.remove(filePath)
                except:
                    print('Error while deleting file: ', filePath)

            fileList = glob.glob(base+"/"+regfile+"/*copy.dom")
            for filePath in fileList:
                try:
                    os.remove(filePath)
                except:
       	            print('Error while deleting file: ', filePath)



print("Done merging",flush=True)
