"""

The PLI and structure tensor data in BigMac have the azimuthal/inplane angle defined
in the opposite sense (i.e. one is clockwise, the other is anticlockwise)

To ensure consistency, we here reverse the inplane angle in the PLI mosaics.
The original mosaic is renamed mosaic_reversed.tif

Created on Nov 14 2021
@author: amyh

"""

import tirl
from tirl.timage import TImage
import os
import sys

# Base folder
base = sys.argv[1]

def reverse(pli_inplane,out):
    inplane = TImage(pli_inplane)
    inplane.normalise()
    inplane = 1-inplane
    inplane.snapshot(out)

# If not already done, reverse image to make it consistent with structure tensor output

if not os.path.isfile(base+"/In_plane/mosaic.tif"):
    reverse(base + "/In_plane/mosaic_reversed.tif",base+"/In_plane/mosaic.tif")

if not os.path.isfile(base+"/In_plane/mosaic_reversed.tif"):
    os.rename(base+"/In_plane/mosaic.tif",base+"/In_plane/mosaic_reversed.tif")
    reverse(base + "/In_plane/mosaic_reversed.tif",base+"/In_plane/mosaic.tif")
