#!/bin/bash

# Mapping PLI pixel coordinates, inplane angle and microscopy normal to dwi b10k space

# Example command
# for i in /vols/Data/BigMac/Microscopy/PLI/Posterior/P???x; do j=${i##*/}; ./TIRLmapping_BigMacMicro.sh ${j} Reg2MRI 100 PLI Posterior; done

# Map tirl parameters in batches of 100 rows at a time - to avoid memory issues.
# AH 25/11/2020, revised 21/11/2021, revised 03/02/2022 for optimised registration and tirlv3

# Note use /opt/fmrib/fsltmp/fsl_0eb1b895/bin/fslpython if calling fslpython

source activate tirlenv_v3.0
#source activate tirlenv_v2.1.3

slideno=$1      # P090x or CV090a etc
regfile=$2      # Reg2MRI
batchsize=$3    # 100
stain=$4        # PLI / Gallyas / Cresyl
half=$5         # Anterior/Posterior
overwrite=$6	# Set to 1 if want to overwrite previous output or it is the first time processing the slide

# Diffusion data to map to i.e. here we map to b10k space
dwi=/vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/data/S0.nii.gz
field=/vols/Data/BigMac/MRI/Postmortem/dwi/b10k/1.0mm/reg/dwi_1.0mm_2_struct_fnirt/dwi_1.0mm_2_struct_fnirt_field.nii.gz
dwiout=dwi_1.0mm_b10kspace      # suffix for output
#dwi=False
#field=False
#dwiout=structspace
dwiout=dwi_1.0mm_b10kspace_correct_rotation

if [[ $stain == "PLI" ]]; then
    ddir=/vols/Data/BigMac/Microscopy/PLI/${half}/${slideno}/
    inplane=${ddir}/Inclination/mosaic.tif
    queue=bigmem.q
else
    ddir=/vols/Data/BigMac/Microscopy/${stain}/${half}/${slideno::-1}x/structureTensor/${slideno}_ST_150/
    inplane=${ddir}/${slideno}_O.tif
    if [[ ! -e $inplane ]]; then
        if [[ -e $ddir/$regfile/VectorMapping/ ]]; then
            rm -r $ddir/$regfile/VectorMapping/
        fi
        exit 1
    fi
    queue=veryshort.q
fi

out=${ddir}/${regfile}/VectorMapping/
mkdir -p ${out}
if [[ $overwrite == 1 ]]; then
    rm -r ${out}/*
    if [[ -d ${ddir}/${regfile}/Left/VectorMapping/ ]]; then
        rm -r ${ddir}/${regfile}/Left/VectorMapping/*
        rm -r ${ddir}/${regfile}/Right/VectorMapping/*
    fi
fi
mkdir -p ${out}/logs

if [[ $stain == "PLI" ]]; then
    # Reverse PLI orientations if they havent previously been computed
    jall=`fsl_sub -q veryshort.q -l ${out}/logs/ -N ${slideno}_reverse_pli python /home/fs0/amyh/func/tirl/TIRLmapping_BigMacPLI_reverse.py $ddir`
    mergequeue=bigmem.q
else
    jall=1
    #jall=$6
    mergequeue=veryshort.q
fi

# Number of rows in original PLI image
H=`identify $inplane | cut -f 3 -d " " | sed s/.*x//`

# Map coords pli vectors to dwi
# process 'batches' of rows at a time
if [[ -f ${out}/jobs_${dwiout} ]]; then rm ${out}/jobs_${dwiout}; fi
for i in $(seq 0 $((H / ${batchsize}))); do
    echo python /home/fs0/amyh/func/tirl/TIRLmapping_BigMacMicro.py ${slideno} ${regfile} $((i * ${batchsize}))  $(($((i+1)) * ${batchsize})) $stain $half $dwi $field $dwiout >> ${out}/jobs_${dwiout}
done

# Merge when all above jobs are finished
if [[ -f ${out}/merge_${dwiout} ]]; then rm ${out}/merge_${dwiout}; fi
echo python /home/fs0/amyh/func/tirl/TIRLmapping_BigMacMicro_merge.py $slideno $regfile $batchsize $stain $half $dwiout >> ${out}/merge_${dwiout}

if [[ $6 != 1 ]]; then	
    jall=`fsl_sub -l ${out}/logs_${dwiout} -q $mergequeue -j $jall -N ${slideno}_premerge -t ${out}/merge_${dwiout}`
fi
jall=`fsl_sub -q $queue -j $jall -l ${out}/logs_${dwiout} -N ${slideno}_map_vectors -t ${out}/jobs_${dwiout}`
fsl_sub -l ${out}/logs_${dwiout} -q $mergequeue -j $jall -N ${slideno}_merge -t ${out}/merge_${dwiout}

echo "scripts submitted"
